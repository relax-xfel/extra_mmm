import os
import numpy as np
import scipy
from scipy import optimize, ndimage
import matplotlib.pyplot as plt
import sys
import mmmUtils as mu
from PIL import Image
from importlib import reload 
import extra_mmm as em
import extra_data
import rossendorfer_farbenliste as rofl
from matplotlib import colors
from matplotlib.ticker import FormatStrFormatter
from datetime import datetime

lineoutDir='./lineouts/'
shotDir='./shots/'  #Directory to store output images
outDir='./overviews/'  #Directory to store output images


from metadata_client import MetadataClient
badshots=[550,583,579,563,569,598,570,637,793,796,798,801,804,837,891]
def init_history():
    delas=np.zeros(2000)*np.nan
    intes=np.zeros(2000)*np.nan
    targets=np.zeros(2000)*np.nan
    mu.dumpPickle([delas,intes,targets],'history_saxs')
    
def draw_history(firstRun,runNo):
    proposal=3129
    title='last'
    lastRun=runNo

    cmap=plt.cm.get_cmap("jet")
    delas,intes,targets=mu.loadPickle('./history_saxs')
    
    for runNo in np.arange(firstRun,lastRun+1):
#        if runNo in badshots:
 #           print('skipping bad {:.0f}'.format(runNo))
  #          continue
        runtype,sample=get_metadata(runNo)
        if runtype==0: continue
        if runtype!='main':continue
        if runNo==472: continue
        if runNo==361: continue
        run = em.open_run(proposal, runNo, data='all')  #Opening the given run
        trainId,shot_ratio,JF4_sum=em.get_shot_trainId_3(run,debug=0)        
        delay,tr=em.get_array(run,'delay',trainId=trainId)
        delay_c=correct_delay(runNo,delay)
        plx,prx,plofs2,profs2,num=em.get_last_profile(runNo)#preshot
        plx,prx,plofs,profs,num=em.get_last_profile(runNo+1) # main shot
        if np.size(plx)>1:
            c=1
            ratr=profs/profs2*c
            ratl=plofs/plofs2*c
            maxqq=0.25
            ratr=mu.smooth(ratr,13)
            ratl=mu.smooth(ratl,13)
            #dc=(-1*delay_c+3)/6
            dc=(-1*delay_c+7)/15
            dc=(-1*delay_c+2)/10
            if dc<0:dc=0
            if dc>1:dc=1
            sel=(plx>-0.15)*(plx<-0.08)
            inte=np.mean(ratl[sel])
            intes[runNo]=inte
            delas[runNo]=delay_c
            sample_i=0
            if sample=='W': sample_i=1
            if sample=='Cu': sample_i=2
            targets[runNo]=sample_i
    mu.dumpPickle([delas,intes,targets],'history_saxs')
    sel=(targets==1)
    plt.semilogy(delas[sel],intes[sel],'*',ms=8,label='W',color=[0.2,0.3,0.7])
    sel=(targets==2)
    plt.semilogy(delas[sel],intes[sel],'o',ms=7,label='Cu',color=[0.8,0.4,0])
    plt.semilogy(delas[runNo],intes[runNo],'r*',ms=15,label='this',color=[1,0.5,0.8])
    first=1
    mintime=-51
    maxtime=21
    for run in np.arange(1000):
        if np.isfinite(intes[run]):
            if intes[run]<5e-3:continue
            if delas[run]<mintime: continue
            if delas[run]>maxtime: continue
            plt.text(delas[run]+0.05,intes[run],'{:.0f}'.format(run),fontsize=6)
            if run in badshots:
                lab=''
                if first:
                    lab='bad'
                    first=0                    
                plt.plot([delas[run]],intes[run],'o',ms=7,color=[0.7,0.7,0.7],label=lab)
    
    plt.xlabel('Delay [ps]')
    plt.ylabel('Saxs decay ')
    plt.xlim(mintime, maxtime)
    plt.ylim(10e-3,2)        
    plt.title(title)
    plt.legend()
    plt.grid()
    return intes

def correct_delay(runNo,delay):
        #delay_c=delay+2.34 # this was working on 25. until evening
    if runNo>=770: # W 5um, 27th evening
        delay_c=delay+2.14-0.8+0.1+3.1-1.6
    elif runNo>=694: # W 5um, 27th evening
        delay_c=delay+2.14-0.8+0.1+3.1
    elif runNo>=529:
        delay_c=delay+2.14-0.8+0.1 # this is timing 26. in the afternoon
    elif runNo>=348:
        delay_c=delay+2.14-0.8 # this is timing 26. in the afternoon
    else:
        delay_c=delay+2.14 # this is timing 25. in the evening
        #ipm_opt=ipm_opt*-1
    return delay_c
        


def get_metadata(runNo,debug=0):
        # Necessary configuration variables to establish a connection
    # Go to https://in.xfel.eu/metadata/oauth/applications to make a token for
    # the metadata catalogue.
    user_id = 'Mth7jWXuFt8BLlRTH56Hcd1YoVFZ6upm1a2o7yCBgI0'
    user_secret = 'CUsEZe4m-IfRyahjFqRZEO2FtvSyKYKJmyHFLXgyjEQ'
    user_email = 'm.smid@hzdr.de'
    #
    metadata_web_app_url = 'https://in.xfel.eu/metadata'
    token_url = 'https://in.xfel.eu/metadata/oauth/token'
    refresh_url = 'https://in.xfel.eu/metadata/oauth/token'
    auth_url = 'https://in.xfel.eu/metadata/oauth/authorize'
    scope = 'jupyter'
    base_api_url = 'https://in.xfel.eu/metadata/api/'

    # Generate the connection (example with minimum parameter options)
    client_conn = MetadataClient(client_id=user_id,
                                 client_secret=user_secret,
                                 user_email=user_email,
                                 token_url=token_url,
                                 refresh_url=refresh_url,
                                 auth_url=auth_url,
                                 scope=scope,
                                 base_api_url=base_api_url)
    runtypes={}
    runtypes[3285]='TestDAQ'
    runtypes[3286]='Calibration'
    runtypes[3287]='edge scan'
    runtypes[3288]='JF0'
    runtypes[3289]='JF1'
    runtypes[3290]='JF2'
    runtypes[3291]='main'
    runtypes[3292]='post'
    runtypes[3293]='pre'
    runtypes[3294]='x-ray only'

    samples={}
    samples[3617]='Carbon'
    samples[3618]='Cu'
    samples[3619]='Silicon'
    samples[3620]='coated'
    samples[3621]='Al'
    samples[3622]='W'
    samples[3681]='nanospheres80'
    samples[3682]='nanospheres20'
    samples[3683]='YAG'
    samples[3737]='Ni'


    proposal_number=3129
    #runNo=376
    resp = client_conn.get_proposal_runs(proposal_number,page=runNo,page_size=1)
    #print(resp)
    if np.size(resp['data']['runs'])==0:
        return 0,0
    run=resp['data']['runs'][0]
    if debug: print (run)
    rtype=runtypes[run['experiment_id']]
    sample=samples[run['sample_id']]
    return rtype, sample





def overview_figure_3129_A(proposal,runNo,shot_previous):# used until Aug 26 morning
    print('Going to do run #{:.0f}'.format(runNo))
    
    r=5
    c=6
    #axHapgAImage = plt.subplot2grid((r, c), (0, 0), colspan=2, rowspan=1)
    #axHapgBImage = plt.subplot2grid((r, c), (1, 4), colspan=2, rowspan=1)
    #axHapgAImage = plt.subplot2grid((r, c), (2, 4), colspan=2, rowspan=1)
    
    axHapgBImage=[]
    axHapgAImage=[]
    axHistory = plt.subplot2grid((r, c), (1, 4), colspan=2, rowspan=2)
    axZyla = plt.subplot2grid((r, c), (3, 0), colspan=2, rowspan=2)
    
    axTrains = plt.subplot2grid((r, c), (0, 4), colspan=2, rowspan=1)
    #axTest = plt.subplot2grid((r, c), (0, 2), colspan=1, rowspan=1)
    axSaxs_back = plt.subplot2grid((r, c), (0, 2), colspan=1, rowspan=1)
    axHello = plt.subplot2grid((r, c), (0, 3), colspan=1, rowspan=1)
    axSaxs = plt.subplot2grid((r, c), (1, 2), colspan=2, rowspan=1)
    axSaxs_rot = plt.subplot2grid((r, c), (2, 2), colspan=2, rowspan=1)
    axSaxs_profile = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axSpectra = plt.subplot2grid((r, c), (3, 4), colspan=2, rowspan=2)
    axInfo = plt.subplot2grid((r, c), (0, 0), colspan=1, rowspan=2)

    col_Ge=rofl.g()
    col_HapgA=rofl.b()
    col_HapgB=rofl.o()
    debug=0
    recalc=1
    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    if run==0: return 0
    #epix=em.get_image(run,'ePix1',trainId,debug=debug,recalc=recalc)
    #trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    if 'HED_IA1_JF500K4/DET/JNGFR04:daqOutput' in run.all_sources:
        trainId,shot_ratio,JF4_sum=em.get_shot_trainId_3(run,debug=0)        
        if shot_ratio<10:
            trainId=-1
    elif 'HED_OPT_IPM/ADC/1:channel_2.output' in run.all_sources:
        trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    else: trainId=-1
    shot=(trainId!=-1)

    r,s=get_metadata(runNo)
    text2='runtype: {:s}, sample: {:s}'.format(r,s)
    plt.gcf().text(0.1,0.01, text2, fontsize=16,color=[0.4,0.1,0.1])
    
    #postshot= shot_previous and not shot
    postshot=(r=='post')
    preshot=(r=='pre')

    if axInfo!=[]:
        plt.sca(axInfo)

        plt.axis('off')
        em.print_text_rw=0.03
        em.print_text_y=0.9
        plt.gcf().text(0.01, 0.95, "p{:04.0f}".format(proposal),color=[0,0.2,0.5],fontsize=24)
        plt.gcf().text(0.1, 0.95, "#{:03.0f}".format(runNo),color=[0.5,0.2,0.],fontsize=24)
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        plt.gcf().text(0.02, 0.92, "trainId {:.0f} , ratio {:.2e},       overview time: {:s}".format(trainId,shot_ratio,current_time))
        if shot:plt.gcf().text(0.18, 0.95, "shot!",color='w',fontsize=18,backgroundcolor=[0.9,0.7,0.])
        elif postshot: plt.gcf().text(0.18, 0.95, "postshot",color=[0.2,0.2,0.7],fontsize=18)
        elif preshot: plt.gcf().text(0.18, 0.95, "preshot",color=[0.2,0.2,0.7],fontsize=18)
        else:plt.gcf().text(0.18, 0.95, "accumulation",color=[0.0,0.4,0.8],fontsize=18)
        xgm_sa2,tr=em.get_array(run,'XGM_SA2',trainId=trainId)
        att_sa2,tr=em.get_array(run,'ATT_SA2',trainId=trainId)
        xgm_hed,tr=em.get_array(run,'XGM_HED',trainId=trainId)
        att_hed,tr=em.get_array(run,'ATT_HED',trainId=trainId)
        ipm_opt,tr=em.get_IPM_OPT(run,trainId=trainId,debug=0)
        delay,tr=em.get_array(run,'delay',trainId=trainId)
        delay_c=correct_delay(runNo,delay)
        
        em.print_text("XGM SA2 {:s} μJ",xgm_sa2,0)
        em.print_text("ATT_SA2: {:s} %",att_sa2*100,1)
        em.print_text("XGM_HED: {:s} μJ",xgm_hed,2)
        em.print_text("ATT_HED: {:s} %",att_hed*100,3)
        em.print_text("IPM_OPT: {:s} μJ.",ipm_opt,4)
        transmission1=xgm_hed/xgm_sa2/att_sa2
        em.print_text("trans. betw. XGMs: {:s}%.",transmission1*1e2,5,derived=1)

        beamline_trans2=ipm_opt/xgm_sa2/att_sa2/att_hed
        em.print_text("trans. till IPM: {:s}%.",beamline_trans2*1e2,6,derived=1)
        
        row=16
        if shot:
            if np.size(delay)==0:
                pos=text.find('{')
                textnull='no delay'
                col='red'
                plt.gcf().text(0.02,emprint_text_y-em.print_text_rw*row, textnull, fontsize=24,color=col)
            else:
                text2="delay: {:.1f} ps".format(delay_c)
                col='blue'
                plt.gcf().text(0.02,em.print_text_y-em.print_text_rw*row, text2, fontsize=24,color=col)

    if 1:
        plt.sca(axHello)
        import matplotlib.image as mpimg
        if shot:
            img = mpimg.imread('hello.png')
        elif postshot:
            img = mpimg.imread('bye.png')                
        elif preshot:
            img = mpimg.imread('preshot.png')
        else:
            img = mpimg.imread('hellono.png')
            
        plt.imshow(img)
        plt.axis('off')

        
# TRAINS
    if axTrains!=[]:
        ipm_opt_all,tr=em.get_IPM_OPT(run,trainId=0,debug=0)
        if np.size(ipm_opt_all)>1:
            smi=mu.smooth(ipm_opt_all,21)
            trm=tr-tr[0]
            plt.sca(axTrains)
            maxtr=np.nanmax(ipm_opt_all)
            l=plt.plot(trm,ipm_opt_all,'k.')
            plt.ylabel('IPM OPT [~μJ]')
            axTrains.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
            plt.plot(trm,smi,'-',lw=20,zorder=-10,color=[0,0.2,0.8,0.2])
            trnum=np.size(tr)
            ax=plt.gca()
            plt.xlim(0,np.max(trm))
            plt.text(0.3,0.25,'{:.0f} trains total'.format(trnum),transform = ax.transAxes,color='red')
            plt.text(0.3,0.15,'first train:{:.0f}'.format(tr[0]),transform = ax.transAxes,color='red')
    
                #The JF4 evolution
            plt.sca(axTrains)
            JF42=JF4_sum/np.max(JF4_sum)*maxtr
            l=plt.plot(JF42,'rx',label='JF4 int',zorder=-20)
            plt.legend()
    if np.isnan(ipm_opt): ipm_opt=1

        
#SAXS        
    if axSaxs!=[]:
        sh=100
        sw=100

        sh2=370 #full
        sw2=190

        saxs_hor=350
        saxs_ver=360
        lth=6
        if proposal==2621:
            jf4=em.get_image(run,'JF3',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
            saxs_hor=150
            saxs_ver=550
        else:
            jf4=em.get_image(run,'JF4',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
        if np.size(jf4)>1:#SAXS

            rect=np.array([saxs_ver-sh,saxs_ver+sh,saxs_hor-sw,saxs_hor+sw])
            jf=jf4/(1e-6/1.6e-19*1e-3) #keV -> μJ
            SAXS_uJ=np.sum(jf)
            SAXS_uJ_cut=np.sum(mu.cutRect(rect,jf))
            jf=jf/ipm_opt
            
            #print(SAXS_uJ)
            em.print_text("E deposited on whole JF4 {:s} μJ",SAXS_uJ,8,derived=0)
            em.print_text("E deposited on SAXS (white) {:s} μJ",SAXS_uJ_cut,9,derived=0)
            em.print_text(" - ratio to IPM: {:s}",SAXS_uJ_cut/ipm_opt,10,derived=0)

            jf[jf<=0]=1e-20
            plt.sca(axSaxs)

            if np.nansum(jf)!=0:
                plt.imshow(jf,norm=colors.LogNorm(), cmap=rofl.cmap(),aspect='auto')
                #plt.colorbar()
                plt.clim(1e-12,1e-7)
                plt.xlim(0,800)
                plt.ylim(100,512)
                plt.title('SAXS')
                #plt.axis('off')
                
            plt.sca(axSaxs_back)
            jfs=mu.smooth2d(jf4,11)
            plt.imshow(jfs)
            plt.clim(0,100)
            plt.ylim(0,512)
            plt.colorbar()
            saxs_mean=np.mean(jf4)
            plt.title('SAXS mean {:.0f} keV/px'.format(saxs_mean))

         #SAXS profile   
            rot=43   #rotation [°]
            bl=480 #horziontal end of left streak
            br=530 #horziontal end of right streak
            w=14
            axw=50 #height of the figure 
            x0 = 510 #horizontal cener of the lineout [px]
            line=700 #vertical center of the profile image
            maxq=0.4 #just the horizontal axes for plotting
            ax=[280,650,line-axw,line+axw]
            rl=[690,710] #vertical position of left streak (min,max)
            rr=[705,720] #vertical position of right streak (min,max)            
#values after 25.8., 16.07
            rot=45.5
            
            #for run 326+327
            rot=40
            rl=np.array([690,710])-30 #vertical position of left streak (min,max)
            rr=np.array([705,720])-35 #vertical position of right streak (min,max)            
            if runNo>=348:
                rot=46
                rl=np.array([690,710])-5 #vertical position of left streak (min,max)
                rr=np.array([705,720])-5 #vertical position of right streak (min,max)            
            
            # px to Q conversion
            q_conv_H=0.0240*75e-3 #conversion along the gap .... values from raytracing
            q_conv_V=0.0157*75e-3 #conversion perp. to the gap
            #rot=#rotation [°]. If this is set to 0, the gap is vertical, lineout horizontal, so I need to use the "V" conversion
            #If it is 90°, I'd need to use the "H" value.
            r=rot/180*np.pi
            #Believing in math:
            q_conv=((np.cos(r)*q_conv_V)**2 + (np.sin(r)*q_conv_H)**2)**0.5
            
  
            shotr=ndimage.rotate(jf, rot, reshape=True)

            profs=shotr[rr[0]:rr[1],:]
            profs=np.mean(profs,0)
#            profs=profs/np.max(profs)

            plofs=shotr[rl[0]:rl[1],:]
            plofs=np.mean(plofs,0)
#            plofs=plofs/np.max(plofs)

            plt.sca(axSaxs_rot)
            if 1:
                plt.imshow(shotr,cmap=rofl.cmap(),norm=colors.LogNorm())
                plt.clim(1e-12,1e-8)
            #    plt.colorbar()
                plt.xlim(ax[0],ax[1])
                plt.ylim(ax[2],ax[3])
                plt.plot([bl,0,0,bl],[rl[0],rl[0],rl[1],rl[1]],'w-',linewidth=0.5)    
                plt.plot([br,1500,1500,br],[rr[0],rr[0],rr[1],rr[1]],'w-',linewidth=0.5)    

            plt.sca(axSaxs_profile)
            
            plx=(np.arange(np.size(plofs[0:bl]))-x0)*q_conv
            prx=(np.arange(np.size(profs[br:-1]))+br-x0)*q_conv
            sline=plt.semilogy(plx,plofs[0:bl],label='#{:.0f}'.format(runNo))
            plt.semilogy(prx,profs[br:-1],color=sline[0].get_color())
            plofs=plofs[0:bl]
            profs=profs[br:-1]
            mu.dumpPickle([plx,prx,plofs,profs],lineoutDir+'saxs_prof{:04.0f}'.format(runNo))
            plt.title('SAXS profile')
            plx,prx,plofs2,profs2,num=em.get_last_profile(runNo)
            if np.size(plx)>1:
                sline=plt.semilogy(plx,plofs2,label='#{:.0f}'.format(num))
                plt.semilogy(prx,profs2,color=sline[0].get_color())
                if shot:
                    c=1e-8
                    ratr=profs/profs2*c
                    ratl=plofs/plofs2*c
                    maxqq=0.25
                    ratr=mu.smooth(ratr,13)
                    ratl=mu.smooth(ratl,13)
                    sline=plt.semilogy(plx[plx>-maxqq],ratl[plx>-maxqq],color='k',label='shot/preshot')
                    plt.semilogy(prx[prx<maxqq],ratr[prx<maxqq],color='k')
                    plt.plot([-maxqq,maxqq],[c,c],color='k',lw=1)

            plx,prx,plofs,profs,num=em.get_last_profile(runNo,1)
            if np.size(plx)>1:
                sline=plt.semilogy(plx,plofs,label='#{:.0f}'.format(num),lw=0.5)
                plt.semilogy(prx,profs,color=sline[0].get_color(),lw=0.5)

            plx,prx,plofs,profs,num=em.get_last_profile(runNo,2)
            if np.size(plx)>1:
                sline=plt.semilogy(plx,plofs,label='#{:.0f}'.format(num),lw=0.5)
                plt.semilogy(prx,profs,color=sline[0].get_color(),lw=0.5)
                
            plt.legend()
            plt.ylim(1e-14,1e-7)
#            plt.xlim(ax[0],ax[1])
            plt.xlabel('approx. q [Å-1]')
            plt.xlim(-maxq,maxq)
            plt.grid()            
            
# %%HAPG  Forward
    jfA=em.get_image(run,'JF2',trainId,debug=debug,recalc=recalc)
    if np.size(jfA)>1:
        centralY=426
        imHeight=40
        cut_left=0
        spectraHeight=5
        HapgA_x0=828
        HapgA_E0=8047
        HapgA_dispersion=-0.81#eV/px
                #processing
            #jf1=np.transpose(jf1)
        jfA2=jfA[centralY-imHeight:centralY+imHeight,cut_left:-1]
        imH=np.shape(jfA2)[0]
        imW=np.shape(jfA2)[1]
        jf1cut=jfA2[imHeight-spectraHeight:imHeight+spectraHeight]
        spectrum_HapgA=np.nanmean(jf1cut,axis=0)
        spectrum_HapgAs=mu.smooth(spectrum_HapgA,7)
        spectrum_HapgAs=spectrum_HapgAs/np.max(spectrum_HapgAs)
        HapgA_xax=((np.arange(np.size(spectrum_HapgA))-HapgA_x0)*HapgA_dispersion + HapgA_E0)*1e-3
        mu.dumpPickle([HapgA_xax,spectrum_HapgA],lineoutDir+'{:04.0f}_HapgA'.format(runNo))
        if axHapgAImage!=[]:
            plt.sca(axHapgAImage)

                #plotting the image
            plt.imshow(jfA2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mu.draw_horizontal_edges(imHeight,spectraHeight,x1=imW,color='w')
            plt.plot(imH-spectrum_HapgAs*(imH),'w',linewidth=0.5)
            plt.colorbar()
            plt.clim(np.max(jfA2)*1e-2,np.max(jfA2))
            plt.xlim(0,imW)
            plt.ylim(imH,0)
            
            plt.title('Forward spectrometer')
            plt.axis('off')
            
        plt.sca(axSpectra)
        plt.semilogy(HapgA_xax,spectrum_HapgA,label='Forward')
        plt.legend()


    # %% HAPG Backward
    jfB=em.get_image(run,'JF1',trainId,debug=debug,recalc=recalc)
    if np.size(jfB)>1:
        centralY=22
        imHeight=10
        cut_left=0
        spectraHeight=1
        HapgB_x0=2415
        HapgB_E0=8047
        HapgB_dispersion=-0.289/1.095
        jfB2=jfB[centralY-imHeight:centralY+imHeight,cut_left:-1]
        imH=np.shape(jfB2)[0]
        imW=np.shape(jfB2)[1]
        jf1cut=jfB2[imHeight-spectraHeight:imHeight+spectraHeight]
        spectrum_HapgB=np.nanmean(jf1cut,axis=0)
        spectrum_HapgBs=mu.smooth(spectrum_HapgB,7)
        spectrum_HapgBs[spectrum_HapgBs<0]=0
        spectrum_HapgBsn=spectrum_HapgBs/np.max(spectrum_HapgBs)
        HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)*1e-3
        mu.dumpPickle([HapgA_xax,spectrum_HapgB],lineoutDir+'{:04.0f}_HapgA'.format(runNo))

        if axHapgBImage!=[]:
            plt.sca(axHapgBImage)
                #plotting the image
            plt.imshow(jfB2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mu.draw_horizontal_edges(imHeight,spectraHeight,x1=imW,color='w')
            plt.plot(imH-spectrum_HapgBsn*(imH),'w',linewidth=0.5)
            
            plt.xlim(0,imW)
            plt.ylim(imH,0)
            plt.colorbar()
            plt.clim(np.max(jfB2)*1e-2,np.max(jfB2))
            
            #plt.clim(0,6000)
            plt.title('Backward spectrometer')
            plt.axis('off')
        plt.sca(axSpectra)
        plt.title('HAPG spectra')
#            spectrum_HapgBs2=mu.smooth(spectrum_HapgB,7)
        plt.semilogy(HapgB_xax,spectrum_HapgBs,label='Backward')
            #plt.ylim(8e2,5e4)        
        if np.max(spectrum_HapgBs)>1e2:
            plt.ylim(1e2,10e4)
            #plt.vlines([8.047,8.200,8.450,8.905],0,1e5,'k',zorder=-20)
        plt.vlines([8.047,8.264,8.20],0,1e5,'k',zorder=-20)
        plt.xlim(7.9,8.6)
        plt.legend()

    #zyla
    if axZyla!=[]:
        mu.tick('PCI')
        zyla=em.get_image(run,'Zyla',trainId+1,debug=debug,recalc=recalc)           
        if np.size(zyla)>1:
            zylab=zyla[250:2000,500:2000]
            zylab=mu.smooth2d(zylab,11)
            zr=np.reshape(zylab,(1,np.size(zylab)))
            a,b=np.histogram(zr,np.arange(90,200,0.1))
            am=np.argmax(a)
            zyla_back=b[am]
            zyla_max=np.max(zyla)
            zyla=zyla-zyla_back
            plt.sca(axZyla)
            zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
            em.print_text("E deposited on ZYLA {:s} a.u.",zyla_int,12,derived=0)            
            em.print_text(" - ratio to IPM: {:s}",zyla_int/ipm_opt,13,derived=0)
            em.print_text(" - max count: {:s} ",zyla_max,14,derived=0)
            zyla_c=zyla/ipm_opt
            plt.imshow(zyla_c, cmap=rofl.cmap())
#            mz=np.max(zyla_c)
            mz=(np.max(zylab)-zyla_back)/ipm_opt
            maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
            am=np.argmin(np.abs(mz-maxs))
            maxset=maxs[am]
            plt.clim(0,mz*0.8)
            plt.xlim(1000,2000)
            plt.ylim(0,1000)
            plt.colorbar()
            plt.title('Zyla, normalized to IPM')
            
    if axHistory!=[]:
        plt.sca(axHistory)
        draw_history(runNo,runNo)

    plt.tight_layout()        
    fc=[0.92,0.91,0.96]
    if shot:
        plt.savefig(shotDir+'Run_p{:04.0f}_shot_{:s}_delay_{:05.0f}_r{:05.0f}.jpg'.format(proposal,s,delay_c*-100+10000,runNo),dpi=200,facecolor=fc)
#        plt.savefig('./display/01_lastshot.png',dpi=200,facecolor=fc)
        
 #   if preshot:
  #      plt.savefig('./display/00_lastpreshot.png',dpi=200,facecolor=fc)
   # if postshot:
    #    plt.savefig('./display/02_lastpostshot.png',dpi=200,facecolor=fc)
        
    return shot



## second layout for shots focused on PCI
def overview_figure_3129_B(proposal,runNo,shot_previous,add_postshot=0):# used until Aug 26 morning
    print('Going to do run #{:.0f}'.format(runNo))
    facecolor=[0.96,0.93,0.93]
    plt.figure(figsize=(18,10),facecolor=facecolor)
    r=5
    c=8
    #axHapgAImage = plt.subplot2grid((r, c), (0, 0), colspan=2, rowspan=1)
    #axHapgBImage = plt.subplot2grid((r, c), (1, 4), colspan=2, rowspan=1)
    #axHapgAImage = plt.subplot2grid((r, c), (2, 4), colspan=2, rowspan=1)
    
    axHapgBImage=[]
    axHapgAImage=[]
    axHistory = plt.subplot2grid((r, c), (1, 4), colspan=2, rowspan=2)
    axHistory = []
    axZyla_pre = plt.subplot2grid((r, c), (3, 0), colspan=1, rowspan=2)
    axZyla = plt.subplot2grid((r, c), (3, 1), colspan=1, rowspan=2)
    axZyla_post = plt.subplot2grid((r, c), (3, 2), colspan=1, rowspan=2)
    axZyla_profile = plt.subplot2grid((r, c), (3, 3), colspan=2, rowspan=2)
    
    axTrains = plt.subplot2grid((r, c), (0, 6), colspan=2, rowspan=1)
    #axTest = plt.subplot2grid((r, c), (0, 2), colspan=1, rowspan=1)
    axSaxs_back = plt.subplot2grid((r, c), (0, 5), colspan=1, rowspan=1)
    axHello = plt.subplot2grid((r, c), (0, 4), colspan=1, rowspan=1)
    
    axSaxs_pre = plt.subplot2grid((r, c), (1, 2), colspan=2, rowspan=1)
    axSaxs = plt.subplot2grid((r, c), (0, 2), colspan=2, rowspan=1)
    axSaxs_rot = plt.subplot2grid((r, c), (2, 2), colspan=2, rowspan=1)
    axSaxs_profile = plt.subplot2grid((r, c), (2, 6), colspan=2, rowspan=2)
    
    axSpectra = plt.subplot2grid((r, c), (1, 6), colspan=2, rowspan=1)
    axInfo = plt.subplot2grid((r, c), (0, 0), colspan=1, rowspan=2)
    axPCI_profile = plt.subplot2grid((r, c), (4, 6), colspan=2, rowspan=1)

    col_Ge=rofl.g()
    col_HapgA=rofl.b()
    col_HapgB=rofl.o()
    debug=0
    recalc=1
    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    if run==0: return 0
    #epix=em.get_image(run,'ePix1',trainId,debug=debug,recalc=recalc)
    #trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    if 'HED_IA1_JF500K4/DET/JNGFR04:daqOutput' in run.all_sources:
        trainId,shot_ratio,JF4_sum=em.get_shot_trainId_3(run,debug=0)        
        if shot_ratio<10:
            trainId=-1
    elif 'HED_OPT_IPM/ADC/1:channel_2.output' in run.all_sources:
        trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    else: trainId=-1
    shot=(trainId!=-1)

    r,s=get_metadata(runNo)
    text2='runtype: {:s}, sample: {:s}'.format(r,s)
    plt.gcf().text(0.1,0.01, text2, fontsize=16,color=[0.4,0.1,0.1])
    
    #postshot= shot_previous and not shot
    postshot=(r=='post')
    preshot=(r=='pre')

    if axInfo!=[]:
        plt.sca(axInfo)

        plt.axis('off')
        em.print_text_rw=0.03
        em.print_text_y=0.9
        plt.gcf().text(0.01, 0.95, "p{:04.0f}".format(proposal),color=[0,0.2,0.5],fontsize=24)
        plt.gcf().text(0.1, 0.95, "#{:03.0f}".format(runNo),color=[0.5,0.2,0.],fontsize=24)
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        plt.gcf().text(0.02, 0.92, "trainId {:.0f} , ratio {:.2e}, overview time: {:s}".format(trainId,shot_ratio,current_time))
        if shot:plt.gcf().text(0.18, 0.95, "shot!",color='w',fontsize=18,backgroundcolor=[0.9,0.7,0.])
        elif postshot: plt.gcf().text(0.18, 0.95, "postshot",color=[0.2,0.2,0.7],fontsize=18)
        elif preshot: plt.gcf().text(0.18, 0.95, "preshot",color=[0.2,0.2,0.7],fontsize=18)
        else:plt.gcf().text(0.18, 0.95, "accumulation",color=[0.0,0.4,0.8],fontsize=18)
        xgm_sa2,tr=em.get_array(run,'XGM_SA2',trainId=trainId)
        att_sa2,tr=em.get_array(run,'ATT_SA2',trainId=trainId)
        xgm_hed,tr=em.get_array(run,'XGM_HED',trainId=trainId)
        att_hed,tr=em.get_array(run,'ATT_HED',trainId=trainId)
        ipm_opt,tr=em.get_IPM_OPT(run,trainId=trainId,debug=0)
        delay,tr=em.get_array(run,'delay',trainId=trainId)
        delay_c=correct_delay(runNo,delay)
        
        em.print_text("XGM SA2 {:s} μJ",xgm_sa2,0)
        em.print_text("ATT_SA2: {:s} %",att_sa2*100,1)
        em.print_text("XGM_HED: {:s} μJ",xgm_hed,2)
        em.print_text("ATT_HED: {:s} %",att_hed*100,3)
        em.print_text("IPM_OPT: {:s} μJ.",ipm_opt,4)
        transmission1=xgm_hed/xgm_sa2/att_sa2
        em.print_text("trans. betw. XGMs: {:s}%.",transmission1*1e2,5,derived=1)

        beamline_trans2=ipm_opt/xgm_sa2/att_sa2/att_hed
        em.print_text("trans. till IPM: {:s}%.",beamline_trans2*1e2,6,derived=1)
        
        row=16
        if shot:
            if np.size(delay)==0:
                pos=text.find('{')
                textnull='no delay'
                col='red'
                plt.gcf().text(0.02,emprint_text_y-em.print_text_rw*row, textnull, fontsize=24,color=col)
            else:
                text2="delay: {:.1f} ps".format(delay_c)
                col='blue'
                plt.gcf().text(0.02,em.print_text_y-em.print_text_rw*row, text2, fontsize=24,color=col)

# TRAINS
    if axTrains!=[]:
        ipm_opt_all,tr=em.get_IPM_OPT(run,trainId=0,debug=0)
        if np.size(ipm_opt_all)>1:
            smi=mu.smooth(ipm_opt_all,21)
            trm=tr-tr[0]
            plt.sca(axTrains)
            maxtr=np.nanmax(ipm_opt_all)
            l=plt.plot(trm,ipm_opt_all,'k.')
            plt.ylabel('IPM OPT [~μJ]')
            axTrains.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
            plt.plot(trm,smi,'-',lw=20,zorder=-10,color=[0,0.2,0.8,0.2])
            trnum=np.size(tr)
            ax=plt.gca()
            plt.xlim(0,np.max(trm))
            plt.text(0.3,0.25,'{:.0f} trains total'.format(trnum),transform = ax.transAxes,color='red')
            plt.text(0.3,0.15,'first train:{:.0f}'.format(tr[0]),transform = ax.transAxes,color='red')
    
                #The JF4 evolution
            plt.sca(axTrains)
            JF42=JF4_sum/np.max(JF4_sum)*maxtr
            l=plt.plot(JF42,'rx',label='JF4 int',zorder=-20)
            plt.legend()
    if np.isnan(ipm_opt): ipm_opt=1

        
#SAXS        
    if axSaxs!=[]:
        sh=100
        sw=100

        sh2=370 #full
        sw2=190

        saxs_hor=350
        saxs_ver=360
        lth=6
        if proposal==2621:
            jf4=em.get_image(run,'JF3',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
            saxs_hor=150
            saxs_ver=550
        else:
            jf4=em.get_image(run,'JF4',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
        if np.size(jf4)>1:#SAXS

            rect=np.array([saxs_ver-sh,saxs_ver+sh,saxs_hor-sw,saxs_hor+sw])
            jf=jf4/(1e-6/1.6e-19*1e-3) #keV -> μJ
            SAXS_uJ=np.sum(jf)
            SAXS_uJ_cut=np.sum(mu.cutRect(rect,jf))
            jf=jf/ipm_opt
            
            #print(SAXS_uJ)
            em.print_text("E deposited on whole JF4 {:s} μJ",SAXS_uJ,8,derived=0)
            em.print_text("E deposited on SAXS (white) {:s} μJ",SAXS_uJ_cut,9,derived=0)
            em.print_text(" - ratio to IPM: {:s}",SAXS_uJ_cut/ipm_opt,10,derived=0)

            jf[jf<=0]=1e-20
            plt.sca(axSaxs)

            if np.nansum(jf)!=0:
                plt.imshow(jf,norm=colors.LogNorm(), cmap=rofl.cmap(),aspect='auto')
                #plt.colorbar()
                plt.xlim(50,620)
                plt.ylim(200,512)
                plt.title('SAXS, whole signal')
                plt.clim(1e-12,1e-7)
                plt.axis('off')
            plt.sca(axSaxs_back)
            jfs=mu.smooth2d(jf4,11)
            plt.imshow(jfs)
            plt.clim(0,100)
            plt.ylim(0,512)
            plt.colorbar()
            saxs_mean=np.mean(jf4)
            plt.title('SAXS mean {:.0f} keV/px'.format(saxs_mean))

         #SAXS profile   
            rot=43   #rotation [°]
            bl=480 #horziontal end of left streak
            br=530 #horziontal end of right streak
            w=14
            axw=50 #height of the figure 
            x0 = 510 #horizontal cener of the lineout [px]
            line=700 #vertical center of the profile image
            maxq=0.4 #just the horizontal axes for plotting
            ax=[280,650,line-axw,line+axw]
            rl=[690,710] #vertical position of left streak (min,max)
            rr=[705,720] #vertical position of right streak (min,max)            
#values after 25.8., 16.07
            rot=45.5
            
            #for run 326+327
            rot=40
            rl=np.array([690,710])-30 #vertical position of left streak (min,max)
            rr=np.array([705,720])-35 #vertical position of right streak (min,max)            
            if runNo>=348:
                rot=46
                rl=np.array([690,710])-5 #vertical position of left streak (min,max)
                rr=np.array([705,720])-5 #vertical position of right streak (min,max)            
            
            # px to Q conversion
            q_conv_H=0.0240*75e-3 #conversion along the gap .... values from raytracing
            q_conv_V=0.0157*75e-3 #conversion perp. to the gap
            #rot=#rotation [°]. If this is set to 0, the gap is vertical, lineout horizontal, so I need to use the "V" conversion
            #If it is 90°, I'd need to use the "H" value.
            r=rot/180*np.pi
            #Believing in math:
            q_conv=((np.cos(r)*q_conv_V)**2 + (np.sin(r)*q_conv_H)**2)**0.5
            
  
            shotr=ndimage.rotate(jf, rot, reshape=True)

            profs=shotr[rr[0]:rr[1],:]
            profs=np.mean(profs,0)
#            profs=profs/np.max(profs)

            plofs=shotr[rl[0]:rl[1],:]
            plofs=np.mean(plofs,0)
#            plofs=plofs/np.max(plofs)

            plt.sca(axSaxs_rot)
            if 1:
                shotr[shotr<1e-12]=1e-12
                plt.imshow(shotr,cmap=rofl.cmap(),norm=colors.LogNorm())
                plt.clim(1e-12,1e-8)
            #    plt.colorbar()
                plt.xlim(ax[0],ax[1])
                plt.ylim(ax[2],ax[3])
                plt.plot([bl,0,0,bl],[rl[0],rl[0],rl[1],rl[1]],'w-',linewidth=0.5)    
                plt.plot([br,1500,1500,br],[rr[0],rr[0],rr[1],rr[1]],'w-',linewidth=0.5)    
                plt.title('SAXS rotated')

            plt.sca(axSaxs_profile)
            
            plx=(np.arange(np.size(plofs[0:bl]))-x0)*q_conv
            prx=(np.arange(np.size(profs[br:-1]))+br-x0)*q_conv
            sline=plt.semilogy(plx,plofs[0:bl],label='#{:.0f}'.format(runNo))
            plt.semilogy(prx,profs[br:-1],color=sline[0].get_color())
            plofs=plofs[0:bl]
            profs=profs[br:-1]
            mu.dumpPickle([plx,prx,plofs,profs],lineoutDir+'saxs_prof{:04.0f}'.format(runNo))

            plx,prx,plofs2,profs2,num=em.get_last_profile(runNo)
            if np.size(plx)>1:
                sline=plt.semilogy(plx,plofs2,label='#{:.0f}'.format(num))
                plt.semilogy(prx,profs2,color=sline[0].get_color())
                if shot:
                    c=1e-8
                    ratr=profs/profs2*c
                    ratl=plofs/plofs2*c
                    maxqq=0.25
                    ratr=mu.smooth(ratr,13)
                    ratl=mu.smooth(ratl,13)
                    sline=plt.semilogy(plx[plx>-maxqq],ratl[plx>-maxqq],color='k',label='shot/preshot')
                    plt.semilogy(prx[prx<maxqq],ratr[prx<maxqq],color='k')
                    plt.plot([-maxqq,maxqq],[c,c],color='k',lw=1)

#            plx,prx,plofs,profs,num=em.get_last_profile(runNo,1)
 #           if np.size(plx)>1:
  #              sline=plt.semilogy(plx,plofs,label='#{:.0f}'.format(num),lw=0.5)
   #             plt.semilogy(prx,profs,color=sline[0].get_color(),lw=0.5)

            plx,prx,plofs,profs,num=em.get_last_profile(runNo,2)
            if np.size(plx)>1:
                sline=plt.semilogy(plx,plofs,label='#{:.0f}'.format(num),lw=0.5,color=[0.5,0.5,0.5])
                plt.semilogy(prx,profs,color=sline[0].get_color(),lw=0.5)
                
            plt.legend()
            plt.ylim(1e-14,1e-7)
#            plt.xlim(ax[0],ax[1])
            plt.xlabel('approx. q [nm-1]')
            plt.xlim(-maxq,maxq)
            plt.title('SAXS profile')
            plt.grid()            

# SAXS preshot
    if shot:
        run_pre = em.open_run(proposal, runNo-1, data='all')  #Opening the given run
        jf4=em.get_image(run_pre,'JF4',trainId=-1,debug=debug,recalc=recalc,threshold_lower=lth)
        ipm_opt_pre,tr=em.get_IPM_OPT(run_pre,trainId=-1,debug=0)
        if np.size(jf4)>1:
            rect=np.array([saxs_ver-sh,saxs_ver+sh,saxs_hor-sw,saxs_hor+sw])
            jf=jf4/(1e-6/1.6e-19*1e-3) #keV -> μJ
            SAXS_uJ=np.sum(jf)
            SAXS_uJ_cut=np.sum(mu.cutRect(rect,jf))
            jf=jf/ipm_opt_pre
            
            jf[jf<=0]=1e-20
         #SAXS profile   
            shotr=ndimage.rotate(jf, rot, reshape=True)
            plt.sca(axSaxs_pre)
            if 1:
                shotr[shotr<1e-12]=1e-12
                plt.imshow(shotr,cmap=rofl.cmap(),norm=colors.LogNorm())
                plt.clim(1e-12,1e-8)
                plt.xlim(ax[0],ax[1])
                plt.ylim(ax[2],ax[3])
                plt.plot([bl,0,0,bl],[rl[0],rl[0],rl[1],rl[1]],'w-',linewidth=0.5)    
                plt.plot([br,1500,1500,br],[rr[0],rr[0],rr[1],rr[1]],'w-',linewidth=0.5)    
                plt.title('SAXS preshot')
#end of SAXS preshot 


# %%HAPG  Forward
    jfA=em.get_image(run,'JF2',trainId,debug=debug,recalc=recalc)
    if np.size(jfA)>1:
        centralY=426
        imHeight=40
        cut_left=0
        spectraHeight=5
        HapgA_x0=828
        HapgA_E0=8047
        HapgA_dispersion=-0.81#eV/px
                #processing
            #jf1=np.transpose(jf1)
        jfA2=jfA[centralY-imHeight:centralY+imHeight,cut_left:-1]
        imH=np.shape(jfA2)[0]
        imW=np.shape(jfA2)[1]
        jf1cut=jfA2[imHeight-spectraHeight:imHeight+spectraHeight]
        spectrum_HapgA=np.nanmean(jf1cut,axis=0)
        spectrum_HapgAs=mu.smooth(spectrum_HapgA,7)
        spectrum_HapgAs=spectrum_HapgAs/np.max(spectrum_HapgAs)
        HapgA_xax=((np.arange(np.size(spectrum_HapgA))-HapgA_x0)*HapgA_dispersion + HapgA_E0)*1e-3
        mu.dumpPickle([HapgA_xax,spectrum_HapgA],lineoutDir+'{:04.0f}_HapgA'.format(runNo))
        if axHapgAImage!=[]:
            plt.sca(axHapgAImage)

                #plotting the image
            plt.imshow(jfA2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mu.draw_horizontal_edges(imHeight,spectraHeight,x1=imW,color='w')
            plt.plot(imH-spectrum_HapgAs*(imH),'w',linewidth=0.5)
            plt.colorbar()
            plt.clim(np.max(jfA2)*1e-2,np.max(jfA2))
            plt.xlim(0,imW)
            plt.ylim(imH,0)
            
            plt.title('Forward spectrometer')
            plt.axis('off')
            
        plt.sca(axSpectra)
        plt.semilogy(HapgA_xax,spectrum_HapgA,label='Forward')
        plt.legend()


    # %% HAPG Backward
    jfB=em.get_image(run,'JF1',trainId,debug=debug,recalc=recalc)
    if np.size(jfB)>1:
        centralY=22
        imHeight=10
        cut_left=0
        spectraHeight=1
        HapgB_x0=2415
        HapgB_E0=8047
        HapgB_dispersion=-0.289/1.095
        jfB2=jfB[centralY-imHeight:centralY+imHeight,cut_left:-1]
        imH=np.shape(jfB2)[0]
        imW=np.shape(jfB2)[1]
        jf1cut=jfB2[imHeight-spectraHeight:imHeight+spectraHeight]
        spectrum_HapgB=np.nanmean(jf1cut,axis=0)
        spectrum_HapgBs=mu.smooth(spectrum_HapgB,7)
        spectrum_HapgBs[spectrum_HapgBs<0]=0
        spectrum_HapgBsn=spectrum_HapgBs/np.max(spectrum_HapgBs)
        HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)*1e-3
        mu.dumpPickle([HapgA_xax,spectrum_HapgB],lineoutDir+'{:04.0f}_HapgA'.format(runNo))

        if axHapgBImage!=[]:
            plt.sca(axHapgBImage)
                #plotting the image
            plt.imshow(jfB2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mu.draw_horizontal_edges(imHeight,spectraHeight,x1=imW,color='w')
            plt.plot(imH-spectrum_HapgBsn*(imH),'w',linewidth=0.5)
            
            plt.xlim(0,imW)
            plt.ylim(imH,0)
            plt.colorbar()
            plt.clim(np.max(jfB2)*1e-2,np.max(jfB2))
            
            #plt.clim(0,6000)
            plt.title('Backward spectrometer')
            plt.axis('off')
        plt.sca(axSpectra)
        plt.title('HAPG spectra')
#            spectrum_HapgBs2=mu.smooth(spectrum_HapgB,7)
        plt.semilogy(HapgB_xax,spectrum_HapgBs,label='Backward')
            #plt.ylim(8e2,5e4)        
        if np.max(spectrum_HapgBs)>1e2:
            plt.ylim(1e2,10e4)
            #plt.vlines([8.047,8.200,8.450,8.905],0,1e5,'k',zorder=-20)
        plt.vlines([8.047,8.264,8.20],0,1e5,'k',zorder=-20)
        plt.xlim(7.9,8.6)
        plt.legend()

    #zyla PCI
    profilemin=1450
    profilemax=1550
    zyla_xlim=[0,1000]
    zyla_xlim=[0,2160]
    zyla_ylim=[0,1000]
    zyla_ylim=[0,2160]
    
    zmain='r'
    zpre='b'
    zpost='k'
    if axZyla!=[]:
        mu.tick('PCI')
        zyla=em.get_image(run,'Zyla',trainId+1,debug=debug,recalc=recalc)           
        if np.size(zyla)>1:
            zylab=zyla[250:2000,500:2000]
            zylab=mu.smooth2d(zylab,11)
            zr=np.reshape(zylab,(1,np.size(zylab)))
            a,b=np.histogram(zr,np.arange(90,200,0.1))
            am=np.argmax(a)
            zyla_back=b[am]
            zyla_max=np.max(zyla)
            zyla=zyla-zyla_back
            if preshot:
                plt.sca(axZyla_pre)
            elif postshot:
                plt.sca(axZyla_post)
            else:
                plt.sca(axZyla)
            zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
            em.print_text("E deposited on ZYLA {:s} a.u.",zyla_int,12,derived=0)            
            em.print_text(" - ratio to IPM: {:s}",zyla_int/ipm_opt,13,derived=0)
            em.print_text(" - max count: {:s} ",zyla_max,14,derived=0)
            plt.imshow(zyla, cmap=rofl.cmap())
            mz=(np.max(zylab)-zyla_back)
            maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
            am=np.argmin(np.abs(mz-maxs))
            maxset=maxs[am]
            plt.clim(0,mz*0.4)
            plt.xlim(1000,2000)
            plt.ylim(zyla_ylim)
#            plt.colorbar()
            plt.title('Zyla, shot')
            
            plt.sca(axPCI_profile)
            zylap=np.nanmean(zyla[:,profilemin:profilemax],1)
            zylap=zylap/np.max(zylap)
            if preshot:
                plt.plot(zylap,label='pre',color=zpre)
            elif postshot:
                plt.plot(zylap,label='post',color=zpost)
            else:
                plt.plot(zylap,label='main',color=zmain)
            plt.xlim(zyla_xlim)
            plt.title('PCI profile')
            
            plt.sca(axZyla_profile)
            vax=np.arange(np.size(zylap))
            vax=(vax-1100)*6.2e-6
            plt.semilogy(vax,zylap,label='main',color=zmain)
            xx=np.arange(0,2000,91)
#            plt.plot(xx,(xx*0)+1e-2,'k*')
            plt.ylim(1e-3,1.5)
            plt.xlabel("Lingen's Q[nm-1]")
            plt.title('low-Q SAXS')

            
            
    if shot:
        if axZyla_pre!=[]:
            if run==0: return 0
            zyla=em.get_image(run_pre,'Zyla',-1,debug=debug,recalc=recalc)           
            if np.size(zyla)>1:
                zylab=zyla[250:2000,500:2000]
                zylab=mu.smooth2d(zylab,11)
                zr=np.reshape(zylab,(1,np.size(zylab)))
                a,b=np.histogram(zr,np.arange(90,200,0.1))
                am=np.argmax(a)
                zyla_back=b[am]
                zyla_max=np.max(zyla)
                zyla=zyla-zyla_back
                plt.sca(axZyla_pre)
                zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
                plt.imshow(zyla, cmap=rofl.cmap())
                mz=(np.max(zylab)-zyla_back)
                maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
                am=np.argmin(np.abs(mz-maxs))
                maxset=maxs[am]
                plt.clim(0,mz*0.8)
                plt.xlim(1000,2000)
                plt.ylim(zyla_ylim)
             #   plt.colorbar()
                plt.title('Zyla, preshot')
                
                plt.sca(axPCI_profile)
                zylap=np.nanmean(zyla[:,profilemin:profilemax],1)
                zylap=zylap/np.max(zylap)
                plt.plot(zylap,label='pre',color=zpre)
                
                plt.sca(axZyla_profile)
                plt.semilogy(vax,zylap,label='pre',color=zpre)
                
        if add_postshot:
            run = em.open_run(proposal, runNo+1, data='all')  #Opening the given run
            if run==0: return 0
            zyla=em.get_image(run,'Zyla',-1,debug=debug,recalc=recalc)           
            if np.size(zyla)>1:
                zylab=zyla[250:2000,500:2000]
                zylab=mu.smooth2d(zylab,11)
                zr=np.reshape(zylab,(1,np.size(zylab)))
                a,b=np.histogram(zr,np.arange(90,200,0.1))
                am=np.argmax(a)
                zyla_back=b[am]
                zyla_max=np.max(zyla)
                zyla=zyla-zyla_back
                plt.sca(axZyla_post)
                zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
                plt.imshow(zyla, cmap=rofl.cmap())
                mz=(np.max(zylab)-zyla_back)
                maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
                am=np.argmin(np.abs(mz-maxs))
                maxset=maxs[am]
                plt.clim(0,mz*0.8)
                plt.xlim(1000,2000)
                plt.ylim(zyla_ylim)
              #  plt.colorbar()
                plt.title('Zyla, postshot')     

                plt.sca(axPCI_profile)
                zylap=np.nanmean(zyla[:,profilemin:profilemax],1)
                zylap=zylap/np.max(zylap)
                plt.plot(zylap,label='post',color=zpost,lw=1)
                
        plt.sca(axPCI_profile)
        plt.legend()

    if 1:
        plt.sca(axHello)
        import matplotlib.image as mpimg
        if shot:
            if add_postshot:
#                if np.max(spectrum_HapgBs)>5e3:
                print(np.mean(spectrum_HapgBs))
                if np.mean(spectrum_HapgBs)>150:
                    img = mpimg.imread('./files/hello2.png')                
                else:
                    img = mpimg.imread('./files/bye2.png')
            else:
                img = mpimg.imread('./files/hello.png')
        elif postshot:
            img = mpimg.imread('./files/bye.png')                
        elif preshot:
            img = mpimg.imread('./files/preshot.png')
        else:
            img = mpimg.imread('./files/hellono.png')
            
        plt.imshow(img)
        plt.axis('off')
    

    if axHistory!=[]:
        plt.sca(axHistory)
        draw_history(runNo,runNo)

    plt.tight_layout()        
    plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)
    if shot:
        plt.savefig(shotDir+'Run_p{:04.0f}_shot_{:s}_delay_{:05.0f}_r{:05.0f}.jpg'.format(proposal,s,delay_c*-100+10000,runNo),dpi=200,facecolor=facecolor)
        
    if 0:
        plt.figure(figsize=(18,10),facecolor=facecolor)
        overview_figure_3129_B(proposal,runNo-1,1,add_postshot=1)# now do the main shot including picutre of this postshot
        plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}.jpg'.format(proposal,runNo-1),dpi=200,facecolor=facecolor)
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)    

    plt.show()


## SAXS only
def overview_figure_3129_C(proposal,runNo,shot_previous,add_postshot=0):# used until Aug 26 morning
    print('Going to do run #{:.0f}'.format(runNo))
    
    r=5
    c=8
    
    axHapgBImage=[]
    axHapgAImage=[]
    
    axTrains = plt.subplot2grid((r, c), (4, 0), colspan=2, rowspan=1)
    
    axSaxs = plt.subplot2grid((r, c), (0, 2), colspan=6, rowspan=5)
    
    axInfo = plt.subplot2grid((r, c), (0, 0), colspan=1, rowspan=2)

    col_Ge=rofl.g()
    col_HapgA=rofl.b()
    col_HapgB=rofl.o()
    debug=0
    recalc=1
    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    if run==0: return 0
    #epix=em.get_image(run,'ePix1',trainId,debug=debug,recalc=recalc)
    #trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    if 'HED_IA1_JF500K4/DET/JNGFR04:daqOutput' in run.all_sources:
        trainId,shot_ratio,JF4_sum=em.get_shot_trainId_3(run,debug=0)        
        if shot_ratio<10:
            trainId=-1
    elif 'HED_OPT_IPM/ADC/1:channel_2.output' in run.all_sources:
        trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    else: trainId=-1
    shot=(trainId!=-1)

    r,s=get_metadata(runNo)
    text2='runtype: {:s}, sample: {:s}'.format(r,s)
    plt.gcf().text(0.1,0.01, text2, fontsize=16,color=[0.4,0.1,0.1])
    
    #postshot= shot_previous and not shot
    postshot=(r=='post')
    preshot=(r=='pre')

    if axInfo!=[]:
        plt.sca(axInfo)

        plt.axis('off')
        em.print_text_rw=0.03
        em.print_text_y=0.9
        plt.gcf().text(0.01, 0.95, "p{:04.0f}".format(proposal),color=[0,0.2,0.5],fontsize=24)
        plt.gcf().text(0.1, 0.95, "#{:03.0f}".format(runNo),color=[0.5,0.2,0.],fontsize=24)
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        plt.gcf().text(0.02, 0.92, "trainId {:.0f} , ratio {:.2e}, overview time: {:s}".format(trainId,shot_ratio,current_time))
        if shot:plt.gcf().text(0.18, 0.95, "shot!",color='w',fontsize=18,backgroundcolor=[0.9,0.7,0.])
        elif postshot: plt.gcf().text(0.18, 0.95, "postshot",color=[0.2,0.2,0.7],fontsize=18)
        elif preshot: plt.gcf().text(0.18, 0.95, "preshot",color=[0.2,0.2,0.7],fontsize=18)
        else:plt.gcf().text(0.18, 0.95, "accumulation",color=[0.0,0.4,0.8],fontsize=18)
        xgm_sa2,tr=em.get_array(run,'XGM_SA2',trainId=trainId)
        att_sa2,tr=em.get_array(run,'ATT_SA2',trainId=trainId)
        xgm_hed,tr=em.get_array(run,'XGM_HED',trainId=trainId)
        att_hed,tr=em.get_array(run,'ATT_HED',trainId=trainId)
        ipm_opt,tr=em.get_IPM_OPT(run,trainId=trainId,debug=0)
        delay,tr=em.get_array(run,'delay',trainId=trainId)
        delay_c=correct_delay(runNo,delay)
        
        em.print_text("XGM SA2 {:s} μJ",xgm_sa2,0)
        em.print_text("ATT_SA2: {:s} %",att_sa2*100,1)
        em.print_text("XGM_HED: {:s} μJ",xgm_hed,2)
        em.print_text("ATT_HED: {:s} %",att_hed*100,3)
        em.print_text("IPM_OPT: {:s} μJ.",ipm_opt,4)
        transmission1=xgm_hed/xgm_sa2/att_sa2
        em.print_text("trans. betw. XGMs: {:s}%.",transmission1*1e2,5,derived=1)

        beamline_trans2=ipm_opt/xgm_sa2/att_sa2/att_hed
        em.print_text("trans. till IPM: {:s}%.",beamline_trans2*1e2,6,derived=1)
        
        row=16
        if shot:
            if np.size(delay)==0:
                pos=text.find('{')
                textnull='no delay'
                col='red'
                plt.gcf().text(0.02,emprint_text_y-em.print_text_rw*row, textnull, fontsize=24,color=col)
            else:
                text2="delay: {:.1f} ps".format(delay_c)
                col='blue'
                plt.gcf().text(0.02,em.print_text_y-em.print_text_rw*row, text2, fontsize=24,color=col)

# TRAINS
    if axTrains!=[]:
        ipm_opt_all,tr=em.get_IPM_OPT(run,trainId=0,debug=0)
        if np.size(ipm_opt_all)>1:
            smi=mu.smooth(ipm_opt_all,21)
            trm=tr-tr[0]
            plt.sca(axTrains)
            maxtr=np.nanmax(ipm_opt_all)
            l=plt.plot(trm,ipm_opt_all,'k.')
            plt.ylabel('IPM OPT [~μJ]')
            axTrains.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
            plt.plot(trm,smi,'-',lw=20,zorder=-10,color=[0,0.2,0.8,0.2])
            trnum=np.size(tr)
            ax=plt.gca()
            plt.xlim(0,np.max(trm))
            plt.text(0.3,0.25,'{:.0f} trains total'.format(trnum),transform = ax.transAxes,color='red')
            plt.text(0.3,0.15,'first train:{:.0f}'.format(tr[0]),transform = ax.transAxes,color='red')
    
                #The JF4 evolution
            plt.sca(axTrains)
            JF42=JF4_sum/np.max(JF4_sum)*maxtr
            l=plt.plot(JF42,'rx',label='JF4 int',zorder=-20)
            plt.legend()
    if np.isnan(ipm_opt): ipm_opt=1

        
#SAXS        
    if axSaxs!=[]:
        sh=100
        sw=100

        sh2=370 #full
        sw2=190

        saxs_hor=350
        saxs_ver=360
        lth=6
        if proposal==2621:
            jf4=em.get_image(run,'JF3',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
            saxs_hor=150
            saxs_ver=550
        else:
            jf4=em.get_image(run,'JF4',trainId,debug=debug,recalc=recalc,threshold_lower=lth)
        if np.size(jf4)>1:#SAXS

            rect=np.array([saxs_ver-sh,saxs_ver+sh,saxs_hor-sw,saxs_hor+sw])
            jf=jf4/(1e-6/1.6e-19*1e-3) #keV -> μJ
            SAXS_uJ=np.sum(jf)
            SAXS_uJ_cut=np.sum(mu.cutRect(rect,jf))
            jf=jf/ipm_opt
            
            #print(SAXS_uJ)
            em.print_text("E deposited on whole JF4 {:s} μJ",SAXS_uJ,8,derived=0)
            em.print_text("E deposited on SAXS (white) {:s} μJ",SAXS_uJ_cut,9,derived=0)
            em.print_text(" - ratio to IPM: {:s}",SAXS_uJ_cut/ipm_opt,10,derived=0)

            jf[jf<=0]=1e-20
            plt.sca(axSaxs)

            if np.nansum(jf)!=0:
                plt.imshow(jf,norm=colors.LogNorm(), cmap=rofl.cmap(),aspect='auto')
                plt.xlim(50,620)
                plt.ylim(200,512)
                plt.title('SAXS, whole signal')
                plt.clim(1e-12,1e-5)
                plt.colorbar()
                plt.axis('off')

    plt.tight_layout()        
    fc=[0.92,0.91,0.96]
    if shot:
        plt.savefig(shotDir+'Run_p{:04.0f}_shot_{:s}_delay_{:05.0f}_r{:05.0f}.jpg'.format(proposal,s,delay_c*-100+10000,runNo),dpi=200,facecolor=fc)
        plt.savefig('./display/01_lastshot.png',dpi=200,facecolor=fc)
        
    if preshot:
        plt.savefig('./display/00_lastpreshot.png',dpi=200,facecolor=fc)
    if postshot:
        plt.savefig('./display/02_lastpostshot.png',dpi=200,facecolor=fc)
        plt.show()
        
        plt.figure(figsize=(18,10),facecolor=fc)
        overview_figure_3129_B(proposal,runNo-1,1,add_postshot=1)# now do the main shot including picutre of this postshot
        plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}.jpg'.format(proposal,runNo-1),dpi=200,facecolor=fc)
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=fc)    

    return shot
