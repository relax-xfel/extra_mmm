import os
from pathlib import Path
import sys
sys.path.append('../extra_mmm-online')
import numpy as np
import extra_mmm as em
import run_overview_lib_3430 as rol
from damnit_ctx import Variable


# If you have the Karabo device name of the XGM you're interested in, you can
# set the variable below and the 'xgm_intensity' and 'pulses' Variable's will
# use it. By default it's set to the XGM for SASE 2.
xgm_name = "SA2_XTD1_XGM/XGM/DOOCS"


"""
@Variable(title="OverviewFig",data='proc')
def Overview_fig(run):
    print('')
    print('')
    print('   ******    Starting to do overview figure             ****')
    erun=em.init_run(run,verbose=0,proc=1)
    runNo,proposal,proc= em.get_run_info(run,debug=1)
    ret=rol.overview_figure_A(proposal,runNo,generator='damnit_online')
    return ret
    #PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    #lth=6
    #jf=em.get_image(run,'JF4',PPU=PPU_open,threshold_lower=lth)
    #return jf
"""


@Variable(title="PPU", summary="sum")
def PPU(run):
    erun=em.init_run(run,verbose=0)
    a,b,c,=em.get_PPU_open(run,1)
    #suma=np.sum(open)    
    return c
  
@Variable(title="Time")
def timess(run,runNo: "meta#run_number", proposal: "meta#proposal", ):
    a,b,times=rol.get_metadata(runNo)
    return times


@Variable(title="metadata")
def metadata(run,runNo: "meta#run_number", proposal: "meta#proposal", ):
    a,b,time=rol.get_metadata(runNo)
    return "{:s},{:s},{:s}".format(a,b)
    
@Variable(title="Delay", summary="mean")
def Delay(run):    
    erun=em.init_run(run,verbose=0)
    runNo,proposal,proc= em.get_run_info(run,debug=0)
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
#    delay,tr=em.get_array(run,'delay',PPU=PPU_open)
    delay,tr=em.get_array(run,'phaseshifter',PPU=PPU_open)
    delay_c=rol.correct_delay(runNo,delay)
    print('Doing delay now correctly ********************')
    return delay_c
        
@Variable(title="Stat")
def Stat(run,runNo: "meta#run_number", proposal: "meta#proposal", ):
    a,b,c=rol.get_metadata(runNo)
    fig=np.zeros((10,10))
    fig[0,:]=1
    if a=='Calibration':
        c=0.7
        fig[4,4]=c
        fig[5,4]=c
        fig[6,4]=c
        fig[3,5]=c
        fig[7,5]=c
        fig[7,6]=c
        fig[3,6]=c
    if a=='TestDAQ':
        c=0.3
        fig[2:8,4]=c
        fig[2,2:7]=c
    if a=='main':
        c=0.9
        fig[2:8,4]=c
        fig[2:8,8]=c
        fig[3,5]=c
        fig[3,7]=c
        fig[4,6]=c
        fig[4,6]=c
#    if np.size(PPU_open)>2:
 #       img = mpimg.imread(path+'/files/hello.png')        
   # else:
   #     img = mpimg.imread(path+'/files/bye.png')
    return fig

@Variable(title="HAPG",data='proc')
def JF1(run):
    erun=em.init_run(run,verbose=0,proc=1)
    runNo,proposal,proc= em.get_run_info(run,debug=0)
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    print("JF1: now the run is {:}".format(proc))
    lth=0
    jf=em.get_image(run,'JF1',PPU=PPU_open,threshold_lower=lth)
    return jf

@Variable(title="Imager",data='proc')
def JF2(run):
    erun=em.init_run(run,verbose=0,proc=1)
    runNo,proposal,proc= em.get_run_info(run,debug=0)
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    jf=em.get_image(run,'JF2',PPU=PPU_open)
    return jf 

@Variable(title="Zyla",data='raw')
def Zyla(run):
    erun=em.init_run(run,verbose=0,proc=1)
    runNo,proposal,proc= em.get_run_info(run,debug=0)
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    lth=0
    jf=em.get_image(run,'Zyla',PPU=PPU_open,threshold_lower=lth)
    return jf 

@Variable(title="Zyla_clean",data='raw')
def Zyla_Clean(run,runNo: "meta#run_number"):
    a,b,c=rol.get_metadata(runNo)
    if a!='main':    return '-'
    zyla=rol.zyla_make(runNo)
    return zyla

@Variable(title="HAPG_spec",data='proc',summary="max")
def Hapg_spec(run,runNo: "meta#run_number"):
    spec=rol.hapg_make(runNo)
    return spec

@Variable(title="XGM max", summary="max")
def xgm_max(run):
    xgm = run[f"{xgm_name}:output", 'data.intensityTD'].xarray()
    return np.round(xgm[:, np.where(xgm[0] > 1)[0]].sum(axis=1))
    
    """
@Variable(title="delay raw", summary="mean")
def delay_raw(run):
    delay = run[f"HED_HPLAS_HIL/DOOCS/OPTICALDELAY", 'actualPosition.value'].xarray()
    return delay
"""
    
    
@Variable(title="Phase shifter", summary="mean")
def phase_shifter(run):
    delay = run[f"HED_HPLAS_HIL/DOOCS/PHASESHIFTER", 'actualPosition.value'].xarray()
    return delay

"""    
@Variable(title="Pulses")
def pulses(run):
    return int(run[xgm_name, 'pulseEnergy.numberOfBunchesActual'][0].ndarray()[0])
"""

@Variable(title="Trains")
def n_trains(run):
    return len(run.train_ids)
"""
@Variable(title="Run size (TB)")
def raw_size(run):
    # If the run only has one file then it's most likely the virtual overview
    # file, which has negligible size. To get the actual run size we find the
    # run directory and get it's size directly.
    if len(run.files) == 1:
        voview_path = Path(run.files[0].filename)
        run_dir = voview_path.parts[-1].split("-")[1].lower()
        proposal_path = voview_path.parents[2]

        run_path = proposal_path / "raw" / run_dir
        run_size_bytes = sum(f.stat().st_size for f in run_path.rglob('*'))
    else:
        # Otherwise, we just use the files opened by extra-data
        run_size_bytes = 0

        for f in run.files:
            run_size_bytes += os.path.getsize(f.filename)

    return run_size_bytes / 1e12
"""