import os
import numpy as np
import scipy
from scipy import optimize, ndimage
import matplotlib.pyplot as plt
import sys
import mmmUtils as mu
from PIL import Image
from importlib import reload 
import extra_mmm as em
import extra_data
import rossendorfer_farbenliste as rofl
from matplotlib import colors
from matplotlib.ticker import FormatStrFormatter
from datetime import datetime

lineoutDir='./lineouts/'
shotDir='../shots/'  #Directory to store output images
outDir='../overviews/'  #Directory to store output images


from metadata_client import MetadataClient
badshots=[]

def correct_delay(runNo,delay):
        #delay_c=delay+2.34 # this was working on 25. until evening
    #delay_c=delay+1034.986 for optical delay
    delay_c=delay+4279.3003
    if runNo>=120: 
        delay_c=delay_c+1.3
    if runNo>=129: 
        delay_c=delay_c+1.3+0.3
    if runNo>=219: 
        delay_c=delay_c+-3.4

    if runNo>=270: 
        delay_c=delay_c+0.5  #together  4279.3003
    if runNo>=353: 
        delay_c=delay_c+0.97

    if runNo>=532:
        delay_c=delay_c+0.5
        
    delay_c=np.round(delay_c*100)/100
    delay_c=delay_c*-1 #to satisfy Alejandro's condition
    return delay_c
        

def zyla_make(runNo,proposal=3430,darkrun=249,usepreshot=1,preshotoffset=-1):
    a=1000
    b=1500
    c=1000
    d=1600    

    a=700 # bigger raNGES
    b=1800
    c=700
    d=1900    
    run_dark = em.open_run(proposal, darkrun, data='all')  
    zyla_dark=em.get_image(run_dark,'Zyla')
    zyla_dark=zyla_dark[a:b,c:d]

    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    zyla=em.get_image(run,'Zyla',PPU=PPU_open)           
    zyla=zyla[a:b,c:d]
    zyla=zyla-zyla_dark

    if usepreshot:
        run_pre = em.open_run(proposal, runNo+preshotoffset, data='all')  
        PPU_pre,PPU_trains,PPU_open_bool=em.get_PPU_open(run_pre,1)
        zyla_pre=em.get_image(run_pre,'Zyla',PPU=PPU_pre)
        zyla_pre=zyla_pre[a:b,c:d]
#        zyla_pre=mu.smooth2d(zyla_pre,5)
        #zyla_pre=zyla_pre-np.min(zyla_pre)
        zyla_pre=zyla_pre-zyla_dark
        zyla=zyla/zyla_pre
        zyla[np.logical_not(np.isfinite(zyla))]=0
    return zyla
    
def hapg_make(runNo,proposal=3430,full=0):
    run = em.open_run(proposal, runNo, data='all')  
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)    
    jfB=em.get_image(run,'JF1',PPU=PPU_open)
    if np.size(jfB)>1:
        centralY=31
        spectraHeight=4
        HapgB_x0=1450
        HapgB_E0=8047
        HapgB_dispersion=-0.289/1.095*1.13
        jf1cut=jfB[centralY-spectraHeight:centralY+spectraHeight]
        spectrum_HapgB=np.nanmean(jf1cut,axis=0)
        spectrum_HapgBs=mu.smooth(spectrum_HapgB,7)
        spectrum_HapgBs[spectrum_HapgBs<0]=0
        
        if full:
            HapgB_x0=1450
            HapgB_E0=8047
            HapgB_dispersion=-0.289/1.095*1.13
            HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)*1e-3 -0.012
            return HapgB_xax,spectrum_HapgBs
        else:
            #silly processing
            spec=np.log10(spectrum_HapgBs)
            spec=np.flip(spec)
            spec=spec[1200:3000]
            return spec
    else:
        return 0 
    
def init_history():
    history=np.zeros((2000,5))
    mu.dumpPickle(history,'history')
    
    
def write_history(runNo,delay_c,resonance_inte,spatial_offset):
    history=mu.loadPickle('./history')
    history[runNo,:]=[delay_c,resonance_inte,spatial_offset,0,0]
    mu.dumpPickle(history,'history')

    
def draw_history(firstRun,currentRun):
    proposal=3430
    title='last'

    cmap=plt.cm.get_cmap("jet")
    history=mu.loadPickle('./history')
    mindel=0
    maxdel=0
    for runNo in np.arange(firstRun,800):
        
        row=history[runNo,:]
        delay=row[0]
        resonance=row[1]
        if resonance==0:continue
        lab=''
        col='g'
        symbol='o'
        if runNo in badshots:
            lab=''
            col=[0.5]
        if runNo==currentRun:
            lab=''
            col='r'
            
            
        if runNo<631:   # any shots before this one will be shown with tiny light marker, and without the text label    ************************
            col=[0.7,0.7,1]
            symbol='.'
            
            
        else:
            plt.text(delay,resonance+100,"{:.0f}".format(runNo),fontsize=8)
        plt.plot([delay],[resonance],marker=symbol,ms=7,color=col,label=lab)
        if delay<mindel:mindel=delay
        if delay>maxdel:maxdel=delay
    #plt.xlim(20,-20)
    plt.xlim(maxdel*1.1,mindel*1.1)
    
    plt.xlabel('Delay [ps]')
    plt.ylabel('Resonance')
#    plt.xlim(mintime, maxtime)
 #   plt.ylim(10e-3,2)        
    plt.title(title)
  #  plt.legend()
    plt.grid()
    yl=plt.ylim()
    yl=[0,yl[1]]
    plt.ylim(yl)
    #return intes

def get_metadata(runNo,debug=0):
        # Necessary configuration variables to establish a connection
    # Go to https://in.xfel.eu/metadata/oauth/applications to make a token for
    # the metadata catalogue.
    try:
        user_id = 'Mth7jWXuFt8BLlRTH56Hcd1YoVFZ6upm1a2o7yCBgI0'
        user_secret = 'CUsEZe4m-IfRyahjFqRZEO2FtvSyKYKJmyHFLXgyjEQ'
        user_id='xfQ3le8ImnoC5-v92DqqK_Eq2wEN8UdqYYwiUj5qxoU'
        user_secret='ETHQcCs3pQTvSyvtiCr_tNmDJ_y-D3gjlI7bzvb6wUY'
        user_email = 'm.smid@hzdr.de'
        #
        metadata_web_app_url = 'https://in.xfel.eu/metadata'
        token_url = 'https://in.xfel.eu/metadata/oauth/token'
        refresh_url = 'https://in.xfel.eu/metadata/oauth/token'
        auth_url = 'https://in.xfel.eu/metadata/oauth/authorize'
        scope = 'jupyter'
        base_api_url = 'https://in.xfel.eu/metadata/api/'

        # Generate the connection (example with minimum parameter options)
        client_conn = MetadataClient(client_id=user_id,
                                     client_secret=user_secret,
                                     user_email=user_email,
                                     token_url=token_url,
                                     refresh_url=refresh_url,
                                     auth_url=auth_url,
                                     scope=scope,
                                     base_api_url=base_api_url)
        runtypes={}
        runtypes[3837]='TestDAQ'
        runtypes[3926]='Calibration'
        runtypes[3287]='edge scan'
        runtypes[3288]='JF0'
        runtypes[3289]='JF1'
        runtypes[3290]='JF2'
        runtypes[3930]='main'
        runtypes[3928]='Cu foil'
        runtypes[3927]='Co foil'
        runtypes[3929]='Ni foil'

        samples={}
        samples[4275]='generic'


        proposal_number=3430
        #runNo=376
        resp = client_conn.get_proposal_runs(proposal_number,page=runNo,page_size=1)
        if np.size(resp['data']['runs'])==0:
            print('Run not found in metadata')
            return 0,0,0
        run=resp['data']['runs'][0]
        if debug: print (run)
        if run['experiment_id'] in runtypes:
            rtype=runtypes[run['experiment_id']]
        else:
            rtype="{:}".format(run['experiment_id'])
        
        if run['sample_id'] in samples:
            sample=samples[run['sample_id']]
        else:
            sample="{:}".format(run['sample_id'])

        from datetime import datetime
        te=run['end_at'][11:19]
        ts=run['begin_at'][11:19]
        end=datetime.strptime(te,'%H:%M:%S')
        begin=datetime.strptime(ts,'%H:%M:%S')
        duration=end-begin
        durs=str(duration)[2:]
        times=ts+' + '+durs
        return rtype, sample,times
    except Exception as e:
        print('Metadata reading failed:')
        print(str(e))
        return '?','?','?'


def overview_figure_A(proposal,runNo,add_postshot=0,mainshot_only=0,debug=0,generator='', ticking=0,recalc=0):
    runtype,sample,times=get_metadata(runNo)
    if mainshot_only:
        if runtype!='main': 
            print('Run #{:.0f} is not a main shot.'.format(runNo))
            return
    mu.clear_times()
    if ticking: mu.tick('overview start')
            
    print('Going to do run #{:.0f}'.format(runNo))
    facecolor=[0.96,0.93,0.93]
    plt.figure(figsize=(18,10),facecolor=facecolor)
    r=5
    c=8
    
    axZyla = plt.subplot2grid((r, c), (3, 0), colspan=2, rowspan=2)
#    axZyla_pre = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axZyla_pre=[]
    axZyla_ratio = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axHistory = plt.subplot2grid((r, c), (3, 4), colspan=4, rowspan=2)
#    axZyla_post = plt.subplot2grid((r, c), (3, 2), colspan=1, rowspan=1)
 #   axZyla_profile = plt.subplot2grid((r, c), (3, 3), colspan=2, rowspan=2)

    axJF2 = plt.subplot2grid((r, c), (0, 2), colspan=2, rowspan=2)
    axJF2=[]
    axHapgBImage=plt.subplot2grid((r, c), (0, 4), colspan=2, rowspan=2)
    
    axTrains = plt.subplot2grid((r, c), (0, 6), colspan=2, rowspan=1)
    #axTest = plt.subplot2grid((r, c), (0, 2), colspan=1, rowspan=1)
 #   axSaxs_back = plt.subplot2grid((r, c), (0, 5), colspan=1, rowspan=1)
#    axHello = plt.subplot2grid((r, c), (0, 4), colspan=1, rowspan=1)
    
  #  axSaxs_pre = plt.subplot2grid((r, c), (1, 2), colspan=2, rowspan=1)
   # axSaxs_rot = plt.subplot2grid((r, c), (2, 2), colspan=2, rowspan=1)
    #axSaxs_profile = plt.subplot2grid((r, c), (2, 6), colspan=2, rowspan=2)
    
    axSpectra = plt.subplot2grid((r, c), (1, 6), colspan=2, rowspan=2)
    axInfo = plt.subplot2grid((r, c), (0, 0), colspan=1, rowspan=2)
 #   axPCI_profile = plt.subplot2grid((r, c), (4, 6), colspan=2, rowspan=1)

    col_Ge=rofl.g()
    col_HapgA=rofl.b()
    col_HapgB=rofl.o()    
    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    if run==0: return 0
    #epix=em.get_image(run,'ePix1',trainId,debug=debug,recalc=recalc)
    #trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    if ticking: mu.tick('trains')
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    use_PPU=1
#    if 'HED_IA1_JF500K4/DET/JNGFR01:daqOutput' in run.all_sources:
    trainId,shot_ratio,JF4_sum,JF4_trains=em.get_shot_trainId_3(run,debug=0,diag='JF1')        
    if 0:
        if shot_ratio<10:
                trainId=-1
        elif 'HED_OPT_IPM/ADC/1:channel_2.output' in run.all_sources:
            trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
        else: trainId=-1
        #shot=(trainId!=-1)

    trainId=-1
    shot=np.size(PPU_open)==1
    shot=(np.size(PPU_open)>=1)*(np.size(PPU_open)<=2)

    text2='runtype: {:s}, sample: {:s}'.format(runtype,sample)
    plt.gcf().text(0.1,0.01, text2, fontsize=16,color=[0.4,0.1,0.1])
    
    postshot=(runtype=='post')
    preshot=(runtype=='pre')
    if ticking: mu.tick('info')

    if axInfo!=[]:
        plt.sca(axInfo)

        plt.axis('off')
        em.print_text_rw=0.025
        em.print_text_y=0.9
        em.print_text_row=0
        plt.gcf().text(0.01, 0.95, "p{:04.0f}".format(proposal),color=[0,0.2,0.5],fontsize=24)
        plt.gcf().text(0.1, 0.95, "#{:03.0f}".format(runNo),color=[0.5,0.2,0.],fontsize=24)
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        #plt.gcf().text(0.02, 0.92, "trainId {:.0f} , ratio {:.2e}, overview time: {:s}".format(trainId,shot_ratio,current_time))
        plt.gcf().text(0.01, 0.92, "Time: {:s}".format(times),fontsize=16)
        plt.gcf().text(0.18, 0.92, "Overview time: {:s}  {:s}".format(current_time,generator),fontsize=8)
        if shot:plt.gcf().text(0.18, 0.95, "shot!",color='w',fontsize=18,backgroundcolor=[0.9,0.7,0.])
        elif postshot: plt.gcf().text(0.18, 0.95, "postshot",color=[0.2,0.2,0.7],fontsize=18)
        elif preshot: plt.gcf().text(0.18, 0.95, "preshot",color=[0.2,0.2,0.7],fontsize=18)
        else:plt.gcf().text(0.18, 0.95, "accumulation",color=[0.0,0.4,0.8],fontsize=18)
        xgm_sa2,tr=em.get_array(run,'XGM_SA2',PPU=PPU_open,trainId=trainId,debug=debug)
        att_sa2,tr=em.get_array(run,'ATT_SA2',PPU=PPU_open,trainId=trainId)
        xgm_hed,tr=em.get_array(run,'XGM_HED',PPU=PPU_open,trainId=trainId)
        att_hed,tr=em.get_array(run,'ATT_HED',PPU=PPU_open,trainId=trainId)
        IPMcalib='3430'
        ipm_opt,tr=em.get_IPM_OPT(run,PPU=PPU_open,trainId=trainId,debug=0,calibration=IPMcalib)
#        delay,tr=em.get_array(run,'delay',PPU=PPU_open,trainId=trainId)
        delay,tr=em.get_array(run,'phaseshifter',PPU=PPU_open,trainId=trainId)
        delay_c=correct_delay(runNo,delay)
        
        em.print_text("XGM SA2 {:s} μJ",xgm_sa2)
        em.print_text("ATT_SA2: {:s} %",att_sa2*100)
        em.print_text("XGM_HED: {:s} μJ",xgm_hed)
        em.print_text("PPU: open {:s} x",np.size(PPU_open))
        
        em.print_text("ATT_HED: {:s} %",att_hed*100)
        em.print_text("IPM_OPT: {:s} μJ.",ipm_opt)
        transmission1=xgm_hed/xgm_sa2/att_sa2
        em.print_text("trans. betw. XGMs: {:s}%.",transmission1*1e2,derived=1)

        beamline_trans2=ipm_opt/xgm_sa2/att_sa2/att_hed
        em.print_text("trans. till IPM: {:s}%.",beamline_trans2*1e2,derived=1)
        
   #     scanx,tr=em.get_array(run,'SCAN_X',PPU=PPU_open,trainId=trainId)
  #      scany,tr=em.get_array(run,'SCAN_Y',PPU=PPU_open,trainId=trainId)
 #       em.print_text("FSS X: {:s}",scanx,derived=0)
#        em.print_text("FSS Y: {:s}",scanx,derived=0)
        motors=['SCAN_X','SCAN_Y','OAP_X','OAP_Y','OAP_Z']
        for motor in motors:
            mot,tr=em.get_array(run,motor,PPU=PPU_open,trainId=trainId)
            em.print_text("{:s} {:s}".format(motor,'{:s}'),mot,derived=0)
            
        row=16
        #if shot:
        if 1:
            if np.size(delay)==0:
                pos=text.find('{')
                textnull='no delay'
                col='red'
                plt.gcf().text(0.02,emprint_text_y-em.print_text_rw*row, textnull, fontsize=24,color=col)
            else:
                text2="delay: {:.1f} ps".format(delay_c)
                col='blue'
                plt.gcf().text(0.02,em.print_text_y-em.print_text_rw*row, text2, fontsize=24,color=col)

# TRAINS
    if axTrains!=[]:
        if ticking: mu.tick('trains')
        
        ipm_opt_all,tr=em.get_IPM_OPT(run,trainId=0,debug=0,calibration=IPMcalib)
        if np.size(ipm_opt_all)>1:
            smi=mu.smooth(ipm_opt_all,21)
            zerotrain=tr[0]
            trm=tr-zerotrain
            plt.sca(axTrains)
            maxtr=np.nanmax(ipm_opt_all)
            l=plt.plot(trm,ipm_opt_all,'k.',label='IPM')
            plt.ylabel('IPM OPT [~μJ]')
            axTrains.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
            plt.plot(trm,smi,'-',lw=20,zorder=-10,color=[0,0.2,0.8,0.2])
            trnum=np.size(tr)
            ax=plt.gca()
            plt.xlim(0,np.max(trm))
            plt.text(0.3,0.25,'{:.0f} trains total'.format(trnum),transform = ax.transAxes,color='red')
            plt.text(0.3,0.15,'first train:{:.0f}'.format(tr[0]),transform = ax.transAxes,color='red')
    
                #The JF4 evolution
            plt.sca(axTrains)
            if 1:
                JF42=JF4_sum/np.max(JF4_sum)*maxtr
                l=plt.plot(JF4_trains-zerotrain,JF42,'rx',label='JF1',zorder=-20)
                
            yl=plt.ylim()               
            PPU_open_bool=PPU_open_bool*1.
            PPU_open_bool[PPU_open_bool==0]=np.nan            
            plt.plot(PPU_trains-zerotrain,PPU_open_bool*yl[1],'b*',label='PPU')
            yl=np.array([yl[0],yl[1]*1.02])
            plt.ylim(yl)
            
            plt.legend()
    if np.isnan(ipm_opt): ipm_opt=1

    if ticking: mu.tick('saxs')

 
  
# SAXS preshot
    if shot:
        run_pre = em.open_run(proposal, runNo-1, data='all')  #Opening the given run
        PPU_pre=em.get_PPU_open(run_pre,0)
        if 0:
            jf4=em.get_image(run_pre,'JF4',PPU=PPU_pre,debug=debug,recalc=recalc,threshold_lower=lth)
            ipm_opt_pre,tr=em.get_IPM_OPT(run_pre,PPU=PPU_pre,trainId=-1,debug=0,calibration=IPMcalib)
            if np.size(jf4)>1:
                rect=np.array([saxs_ver-sh,saxs_ver+sh,saxs_hor-sw,saxs_hor+sw])
                jf=jf4/(1e-6/1.6e-19*1e-3) #keV -> μJ
                SAXS_uJ=np.sum(jf)
                SAXS_uJ_cut=np.sum(mu.cutRect(rect,jf))
                jf=jf/ipm_opt_pre

                jf[jf<=0]=1e-20
             #SAXS profile   
                shotr=ndimage.rotate(jf, rot, reshape=True)
                plt.sca(axSaxs_pre)
                if 1:
                    shotr[shotr<1e-12]=1e-12
                    plt.imshow(shotr,cmap=rofl.cmap(),norm=colors.LogNorm())
                    plt.clim(1e-12,1e-8)
                    plt.xlim(ax[0],ax[1])
                    plt.ylim(ax[2],ax[3])
                    plt.plot([bl,0,0,bl],[rl[0],rl[0],rl[1],rl[1]],'w-',linewidth=0.5)    
                    plt.plot([br,1500,1500,br],[rr[0],rr[0],rr[1],rr[1]],'w-',linewidth=0.5)    
                    plt.title('SAXS preshot')
#end of SAXS preshot 


    # %% HAPG 
    jfB=em.get_image(run,'JF1',PPU=PPU_open,trainId=trainId,debug=debug,recalc=recalc)
    if np.size(jfB)>1:
        centralY=31
        cut_left=0
        spectraHeight=4
        HapgB_x0=1450
        HapgB_E0=8047
        HapgB_dispersion=-0.289/1.095
        HapgB_dispersion=-0.289/1.095*1.2
        HapgB_dispersion=-0.289/1.095*1.13
        jfB2=jfB
        imH=np.shape(jfB2)[0]
        imW=np.shape(jfB2)[1]
        jf1cut=jfB2[centralY-spectraHeight:centralY+spectraHeight]
        spectrum_HapgB=np.nanmean(jf1cut,axis=0)
        spectrum_HapgBs=mu.smooth(spectrum_HapgB,7)
        spectrum_HapgBs[spectrum_HapgBs<0]=0
        spectrum_HapgBsn=spectrum_HapgBs/np.max(spectrum_HapgBs)
        HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)*1e-3 -0.012-0.006
        mu.dumpPickle([HapgB_xax,spectrum_HapgB],lineoutDir+'{:04.0f}_HapgB'.format(runNo))

        if axHapgBImage!=[]:
            plt.sca(axHapgBImage)
                #plotting the image
            plt.imshow(jfB2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mu.draw_horizontal_edges(centralY,spectraHeight,color='w')
 #           plt.plot(imH-spectrum_HapgBsn*(imH),'w',linewidth=0.5)
            
            plt.xlim(0,2200)
            plt.ylim(10,50)    
            plt.clim(1,10000)
            if np.max(spectrum_HapgBs)>1e2:
                plt.clim(1e1,5e4)

            plt.colorbar()
            plt.title('HAPG spectrometer')
    #        plt.axis('off')
        plt.sca(axSpectra)
        plt.title('HAPG spectra')
        plt.semilogy(HapgB_xax,spectrum_HapgBs,label='Backward')
        selback=(HapgB_xax>7.9)*(HapgB_xax<7.95)
        back=np.mean(spectrum_HapgBs[selback])
        if np.max(spectrum_HapgBs)>1e2:
            plt.ylim(1e2,1e5)
        plt.vlines([8.047,8.027,8.15],0,1e5,'k',zorder=-20)
        plt.xlim(7.9,8.4)
        plt.legend()
        # %% measuring resonance peak
        minE=8.13
        maxE=8.17
        mu.plot_vline_till_plot([minE,maxE],HapgB_xax,spectrum_HapgBs,y0=0,color='g', linewidth=1)
        plt.plot([minE,maxE],[back,back],'g-',linewidth=1)
#        mu.plot_vline_till_plot(maxE,HapgB_xax,spectrum_HapgBs,y0=0,color='g', linewidth=1)

        specback=spectrum_HapgBs-back
        selE=(HapgB_xax>minE)*(HapgB_xax<maxE)
        resonance_inte=np.sum(specback[selE])
        plt.text(8.2,1e4,'{:.0f}'.format(resonance_inte),color='g')
        if 0: # debug hapg make
            spec=hapg_make(runNo)
            plt.semilogy(HapgB_xax,spec,label='test')

        
    if axJF2!=[]:
        if ticking: mu.tick('JF2')
        img=em.get_image(run,'JF2',PPU=PPU_open,trainId=trainId,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            img=mu.smooth2d(img,11)
            plt.sca(axJF2)
            plt.imshow(img, cmap=rofl.cmap(),aspect='auto')
            plt.title('JF2')
            
    if ticking: mu.tick('zyla')
    #zyla PCI
    zmain='r'
    zpre='b'
    zpost='k'
    if axZyla!=[]:
        if ticking: mu.tick('PCI')
        zyla=em.get_image(run,'Zyla',PPU=PPU_open,trainId=trainId,debug=debug,recalc=recalc)           
        if np.size(zyla)>1:
            zylab=zyla[250:2000,500:2000]
            zylab=mu.smooth2d(zylab,11)
            zr=np.reshape(zylab,(1,np.size(zylab)))
            a,b=np.histogram(zr,np.arange(90,200,0.1))
            am=np.argmax(a)
            zyla_back=b[am]
            zyla_max=np.max(zyla)
            zyla=zyla-zyla_back
            if preshot:
                plt.sca(axZyla_pre)
 #           elif postshot:
#                plt.sca(axZyla_post)
            else:
                plt.sca(axZyla)
            zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
            em.print_text("E deposited on ZYLA {:s} a.u.",zyla_int,13,derived=0)            
            em.print_text(" - ratio to IPM: {:s}",zyla_int/ipm_opt,14,derived=0)
#            em.print_text(" - max count: {:s} ",zyla_max,15,derived=0)
            plt.imshow(zyla, cmap=rofl.cmap())
            mz=(np.max(zylab)-zyla_back)
            maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
            am=np.argmin(np.abs(mz-maxs))
            maxset=maxs[am]
            plt.clim(0,mz*1)
            plt.xlim(500,1500)
            plt.ylim(500,1500)
            plt.title('Zyla, shot')

    if ticking: mu.tick('PCI -preshot ')          
    if shot:
        if axZyla_pre!=[]:
            if run==0: return 0
            zyla_pre=em.get_image(run_pre,'Zyla',PPU=PPU_pre,debug=debug,recalc=recalc)           
            if np.size(zyla)>1:
                plt.sca(axZyla_pre)
                plt.imshow(zyla_pre, cmap=rofl.cmap())
                plt.xlim(500,1500)
                plt.ylim(500,1500)            
                plt.title('Zyla, preshot')
                
        if ticking: mu.tick('PCI - made nice')
        if axZyla_ratio!=[]:
            zyla_made=zyla_make(runNo)
            if np.size(zyla_made)>1:
                plt.sca(axZyla_ratio)
                plt.imshow(zyla_made, cmap=rofl.cmap())
                plt.colorbar()
                perc=np.percentile(zyla_made,95)
                plt.clim(0,perc)

#                plt.clim(0,mz*0.8)
 #               plt.xlim(500,1500)
  #              plt.ylim(500,1500)            
                plt.title('Zyla, made nice')
                
        if ticking: mu.tick('PCI -postshot')
        if add_postshot:
            run_post = em.open_run(proposal, runNo+1, data='all')  #Opening the given run
            PPU_post=em.get_PPU_open(run_post,0)
            if run==0: return 0
            zyla=em.get_image(run_post,'Zyla',PPU=PPU_post,debug=debug,recalc=recalc)           
            if np.size(zyla)>1:
                zylab=zyla[250:2000,500:2000]
                zylab=mu.smooth2d(zylab,11)
                zr=np.reshape(zylab,(1,np.size(zylab)))
                a,b=np.histogram(zr,np.arange(90,200,0.1))
                am=np.argmax(a)
                zyla_back=b[am]
                zyla_max=np.max(zyla)
                zyla=zyla-zyla_back
                plt.sca(axZyla_post)
                zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
                plt.imshow(zyla, cmap=rofl.cmap())
                mz=(np.max(zylab)-zyla_back)
                maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
                am=np.argmin(np.abs(mz-maxs))
                maxset=maxs[am]
                plt.clim(0,mz*0.8)
                plt.xlim(1000,2000)
                plt.ylim(zyla_ylim)
                plt.title('Zyla, postshot')     

                plt.sca(axPCI_profile)
                zylap=np.nanmean(zyla[:,profilemin:profilemax],1)
                zylap=zylap/np.max(zylap)
                plt.plot(zylap,label='post',color=zpost,lw=1)
                
#        plt.sca(axPCI_profile)
 #       plt.legend()
    if ticking: mu.tick('ending')

    if 0:
        plt.sca(axHello)
        import matplotlib.image as mpimg
        if shot:
            if add_postshot:
#                if np.max(spectrum_HapgBs)>5e3:
                print(np.mean(spectrum_HapgBs))
                if np.mean(spectrum_HapgBs)>150:
                    img = mpimg.imread('./files/hello2.png')                
                else:
                    img = mpimg.imread('./files/bye2.png')
            else:
                img = mpimg.imread('./files/hello.png')
        elif postshot:
            img = mpimg.imread('./files/bye.png')                
        elif preshot:
            img = mpimg.imread('./files/preshot.png')
        else:
            img = mpimg.imread('./files/hellono.png')
            
        plt.imshow(img)
        plt.axis('off')
    
    spatial_offset=0
    if shot:
        write_history(runNo,delay_c,resonance_inte,spatial_offset)

    if axHistory!=[]:
        plt.sca(axHistory)
        draw_history(537,runNo)

    
    plt.tight_layout()        
    plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}_{:s}.jpg'.format(proposal,runNo,generator),dpi=200,facecolor=facecolor)
    if generator.find('o')>=0:
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)    
    if shot:
        plt.savefig(shotDir+'Shot_r{:05.0f}.jpg'.format(runNo),dpi=200,facecolor=facecolor)
        plt.savefig(shotDir+'Shot_last.jpg'.format(delay_c*-100+100000,runNo),dpi=200,facecolor=facecolor)
        
    if 0:
        plt.figure(figsize=(18,10),facecolor=facecolor)
        overview_figure_A(proposal,runNo-1,1,add_postshot=1)# now do the main shot including picutre of this postshot
        plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}.jpg'.format(proposal,runNo-1),dpi=200,facecolor=facecolor)
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)    

    #plt.show()
    mu.print_times()
        
    return 'Ok'
