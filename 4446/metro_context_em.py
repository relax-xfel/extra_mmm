"""
# Context file for Cryojet proposal 4446 at HED instrument, EuXFEL
#### Oliver Humphries, 2023-08-30

modifided to be used outside of metro by Michal Smid
"""

"""
Things to fix:

-[x] Infiniband connection via data aggregator
-[ ] Markers/crosshair on XFEL/Relax pointing for 0,0 and Most recent shot
-[ ] Particle/laser reflection diagnostics
-[x] Cam1_mask
-[x] Fix background subtraction to be after casting to [0, 255] range
"""

"""
## Imports
"""
import os
import cv2
import numpy as np
import xarray as xr
from skimage.measure import LineModelND, ransac
from skimage.draw import line_aa
from collections import deque
from matplotlib.cm import get_cmap
from pathlib import Path

cmap = get_cmap('inferno')
parent_dir = Path('/gpfs/exfel/exp/HED/202302/p004446/usr/Shared/p4446-jet/Online-data-visualization/jet-pointing')
    # Only used for looking up image masks
    
"""
## Source definitions
"""

cam1_name = 'HED_EXP_PPT/CAM/CAM_1:daqOutput@HED_DAQ_DATA/DA/8:output'
cam2_name = 'HED_IA1_LT/CAM/FSI:daqOutput@HED_DAQ_DATA/DA/8:output'
cam3_name = 'HED_IA1_LT/CAM/CAM_3:daqOutput@HED_DAQ_DATA/DA/8:output'

"""
## Metro views
"""
class Imager():
    """Basic image processor class, with simple mask/triggered background capabilities
    """
    mask_file = ""
        # mask file name to be used - uses same directory
    roi_x = slice(0, -1)
    roi_y = slice(0, -1)
        # Area of image to process
    rot90 = 0
        # Number of times to rotate image
    fliplr  = False
        # flips image left-right
    flipud = False
        # flips image up-down
    reshape = True
        # in case daq output is incorrectly shaped, resizes data to the shape of it's transpose
    blur_kernel = 81
        # Blurring size used for image normalization
    
    def __init__(self, source, prefix:str=None, key='data.image.pixels'):
        prefix = prefix if prefix is not None else f"{source.partition(':')[0]}/"
        source, _, loc = source.partition('@')
        loc = ('@' + loc) if loc else loc
#        super().__init__(prefix=prefix, source=source, key=key, loc=loc)
        self._mask = None
    
    def data_shape(self, data: '{source}[{key}]{loc}'):
        """The (uncorrected) shape of the data - i.e. before any rotation/reshape operation
        """
        return data.shape
    
    def raw(self, data: '{source}[{key}]{loc}'):
        """Raw detector image for selected ROI and flip/rotation operations
        """
        if self.reshape:
            data = data.reshape(data.shape[::-1])
        if self.rot90:
            data = np.rot90(data, k=self.rot90)
        if self.fliplr:
            data = np.fliplr(data)
        if self.flipud:
            data = np.flipud(data)
        data = xr.DataArray(data.astype(int), dims=['y', 'x'], coords={'x': np.arange(data.shape[1]), 'y': np.arange(data.shape[0])})
        data = data[self.roi_y, self.roi_x]
        return data

    def background(self, im:'{prefix}raw'):
        """Trigger to use current frame as a background
        """
        return im
    
    def mask(self, data_shape: '{prefix}data_shape'):
        """Cached image mask
        Mask file must be defined at configure! File should be in the folder defined as `parent_folder` above
        The same image transformations will be applied to the data (except reshape)
        """
        data_shape = data_shape[::-1] if self.reshape else data_shape
        mask_file_loc = parent_dir / self.mask_file
        if self._mask is None:
            if os.path.isfile(mask_file_loc):
                self._mask = np.load(mask_file_loc)
                if self._mask.dtype != np.uint8:
                    raise ValueError("Provided mask must be of data type 'uint8'")
                if self._mask.shape != tuple(data_shape):
                    raise RuntimeError(f"Mask data shape ({self._mask.shape}) does not match data ({data_shape})")
            elif self.mask_file == '':
                self._mask = np.zeros(tuple(data_shape), dtype=np.uint8)
            else:
                raise ValueError(f'Invalid mask file: {self.mask_file}')    
            if self.rot90:
                self._mask = np.rot90(self._mask, k=self.rot90)
            if self.fliplr:
                self._mask = np.fliplr(self._mask)
            if self.flipud:
                self._mask = np.flipud(self._mask)
        return self._mask[self.roi_y, self.roi_x].astype(bool)

    def bg_subtracted(self, im:'{prefix}raw', mask:'{prefix}mask', bg:'{prefix}background'=None):
        """Background subtracted image
        If no background is triggered, simply uses raw image
        """
        data = im.data.copy()
        if bg is not None:
            data = data - bg.data
        data *= ~mask
        data = cv2.normalize(data, None, 255, 0, cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        return xr.DataArray(data.astype(np.uint8),dims=im.dims, coords=im.coords)#mask.astype(np.uint8)
    
    def blur(self, data: '{prefix}bg_subtracted'):
        """Blurred image used in normalization
        """
        smooth = cv2.blur(data.data, (self.blur_kernel, self.blur_kernel), 0)
        blur = cv2.divide(data.data, smooth, scale=255)
        #blur = cv2.GaussianBlur(blur, (5, 5), 0)
        return xr.DataArray(blur,dims=data.dims, coords=data.coords)
        
        
    def threshold(self, blur: '{prefix}blur', mask: '{prefix}mask'):
        """Image threshold using Otsu's thresholding algorithm, returns boolean mask
        """
        res, threshold = cv2.threshold(blur.data.ravel()[~mask.ravel()], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        return xr.DataArray((blur < res) & (~mask), dims=blur.dims, coords=blur.coords)

    def foreground(self, data: '{prefix}bg_subtracted', threshold: '{prefix}threshold'):
        """Calculated image foreground as a result of dynamic threshold
        Primary feature here should be the jet, with some background noise still permissable
        Jet should ideally be centred
        """
        return data * threshold.astype(bool)
    
    

class JetPosition(Imager):
    beam_pixel_height = 0
    beam_pixel_x = 0
    line_offset = 0
    um_per_pixel = 1.
    jet_det_thresh = 1.
        # Beam pixel positons on the camera
    
    def _median_foreground(self, foreground: '{prefix}foreground'):
        """Finds the average location of the foreground, in the x-direction
        i.e. tries to find the jet postion
        """
        return foreground.x.where(foreground != 0).median(dim='x')


    def line_fit(self, med: '{prefix}_median_foreground'):
        """Fits a line to the extracted jet position data
        """
        data = np.column_stack([med.data, med.y.data])
        model, _ = ransac(data, LineModelND, min_samples=2, residual_threshold=5, max_trials=1000)
        return model
    
    def perpendicular_distance(self, model:'{prefix}line_fit', raw: '{prefix}bg_subtracted'):
        """Defines an image of perpendicular distance to jet
        """
        origin, direction = model.params
        perp_dir = 1 / np.array([direction[0], -direction[1]])
        perp_dir = perp_dir / np.linalg.norm(perp_dir)
        line = xr.ones_like(raw.y) * model.predict_x(raw.y.data)
        perp_dist = abs((line - raw.x)/perp_dir[0]).transpose('y', 'x', ...)
        return perp_dist
        
        
    def jet_absorption(self, im:'{prefix}bg_subtracted', perp_dist:'{prefix}perpendicular_distance', mask:'{prefix}mask'):
        """Finds the average absorption factor of the fitted jet, scaled to the standard deviation of the illumination outside
        """
        im = im.astype(int).where(~mask)
        jet_int = im.where(perp_dist < self.line_offset).mean(dim='x')
        jet_out_m = im.where(perp_dist > self.line_offset).mean(dim='x')
        jet_out_s = im.where(perp_dist > self.line_offset).std(dim='x')
        cond = ((jet_int - jet_out_m) / jet_out_s)
        cond = cond.where(~np.isnan(cond), 0)
        return cond
    
    def jet_present(self, jet_abs:'{prefix}jet_absorption'):
        return jet_abs.where(jet_abs!=0).mean(dim='y') < -self.jet_det_thresh
    
    def jet_fit(self, raw:'{prefix}bg_subtracted', perp_dist:'{prefix}perpendicular_distance'=None, jet_present:'{prefix}jet_present'=False):
        """Primary image output - showing the background corrected & masked image along
        with the extracted jet fit
        """
        data = raw.copy()
        if jet_present:
            line_offset = set((self.line_offset, -self.line_offset))
            for offset in line_offset:
                data = data.where(abs(perp_dist-offset) < 1, data + np.uint8(128))
        data.attrs.update({'hlines': {self.beam_pixel_height: 'Beam y'},
                           'vlines': {self.beam_pixel_x: 'Beam x'}})
        return data
        
    def jet_x(self, model: '{prefix}line_fit', jet_present: '{prefix}jet_present'):
        """Utility function to return jet offset from defined beam position (assuming it's continuous in y)
        """
        if jet_present:
            return (model.predict_x(np.array([self.beam_pixel_height])) - self.beam_pixel_x) * self.um_per_pixel    
        
    def _position_history(self, x:'{prefix}jet_x'):
        """
        """
        return x + self.beam_pixel_x
    
    def history_center(self, hist: '{prefix}_position_history'):
        counts = np.cumsum(hist.values)
        total = counts[-1]
        target = total // 2
        idx = np.searchsorted(counts, target)
        return hist['bin'].values[idx]

    def position_history(self, hist: '{prefix}_position_history', center: '{prefix}_history_center'):
        """Histogram view of jet position offset, with average and beam position labels
        """
        return xr.DataArray(hist.data, coords=hist.coords,
                            attrs={'vlines': {center: f'Median ({center})',
                                              self.beam_pixel_x: f'Beam center ({self.beam_pixel_x})'}})
    

class BeamPointing():
    """Extracted 2D jet pointing relative to defined beam using 2 cameras    
    """
    CAM_1_angle = 45.
    CAM_2_angle = 135.
    allowed_hit_deviation = 10
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs,
                 tod_source = 'HED_PLAYGROUND/MDL/RelaxTriggerStable',
                 modulo_rem = 'laserTrigger.moduloRemainder',
                 modulo_div = 'laserTrigger.moduloDivisor',
                 loc='')

        # For rate calculations.
        self._hits_acc = 0
        self._hits_last = -1.0
        self._hits_in_run = 0
        self._points_in_run = 0
        self.first_train = 0
        self._relax_pointing_history = deque(maxlen=100)
        self._run_num = 0
        self._run_num2 = 0
    
    def relax_pointing(self,
           cam1_x:'OAP/jet_x',
           cam2_x:'TSO/jet_x'):
        """Combined and scaled jet pointing in 2D
        """
        return cam1_x, cam2_x
    
    def histogram(self, data:'{prefix}Relax_pointing'):
        """Accumulated histogram of jet pointing data
        """
        return xr.DataArray(data, dims=['cam_1', 'cam_2'], attrs={'units': chr(0x03bc) + 'm',
                                                                  'ylabel': 'CAM_1',
                                                                  'xlabel': 'CAM_2'})

    def image(self, im: '{prefix}pointing_data', xy:'{prefix}Relax_pointing'):
        """Decorated hisogram plot with most recent shot data
        """
        im = np.log10(1+im.copy())
        xy = xy.squeeze()
        im.attrs['markers'] = {(xy[0], xy[1]): 'Last shot'}
        im.attrs['hlines'] = {0: 'center'}
        im.attrs['vlines'] = {0: 'center'}
        im.attrs['ellipses'] = {(0, 0, self.allowed_hit_deviation, self.allowed_hit_deviation): 'Allowed'}
        #im = im.rename('$Log_{10}(1+counts)$')
        im.x.attrs['units'] = chr(0x03bc) + 'm'
        im.y.attrs['units'] = chr(0x03bc) + 'm'
        im = im.rename({'x': 'OAP', 'y': 'TSO'})
        return im
    
    def xfel_pointing(self, xy:'{prefix}Relax_pointing'):
        """Pointing coordinates rotated into XFEL coordinate axes, using defined camera positions
        """
        rad1 = np.deg2rad(self.CAM_1_angle)
        rad2 = np.deg2rad(self.CAM_2_angle)
        mat = np.array([[np.cos(rad1), np.sin(rad1)],
                        [np.cos(rad2), np.sin(rad2)]])
        res = np.linalg.solve(mat, xy.T).T
        return res[0,0], res[0, 1]
    
    def laser_shot(self, tID:'internal#train_id',
                   mod_rem:'{tod_source}.{modulo_rem}{loc}',
                   mod_div:'{tod_source}.{modulo_div}{loc}'):
        """Laser shot condition, read from train on demand
        """
        if (tID % mod_div) == mod_rem:
            return True
        
    
    def pointing_condition(self, xy:'{prefix}Relax_pointing'):
        """Condition for a `good` shot, defined such that the 2D offset is < allowed_deviation parameter
        """
        if np.linalg.norm(np.array(xy)) < self.allowed_hit_deviation:
            return True
    
    def hit_condition(self, pointing:'{prefix}pointing_condition',
                      shot:'{prefix}laser_shot'):
        """ Pointing is good *AND* laser was shot
        """
        return (pointing and shot)
    
    def points_in_run(self, pointing_condition:'{prefix}pointing_condition',
                      tID:'internal#train_id',
                      run_num:'HED_DAQ_DATA/DM/RUN_CONTROL.runNumber'=0):
        """Cumulative number of images with `good` pointing since last DAQ run/reconfigure
        """
        if run_num != self._run_num:
            self._points_in_run = 0
            self._run_num = run_num
        self._points_in_run += pointing_condition
        return self._points_in_run

    def hits_in_run(self, tID:'internal#train_id',
                    hit_condition:'{prefix}hit_condition'=0,
                    run_num:'HED_DAQ_DATA/DM/RUN_CONTROL.runNumber'=0):
        """Cumulative number of images with `good` hits since last DAQ run/reconfigure
        """
        if run_num != self._run_num2:
            self._hits_in_run = 0
            self._run_num2 = run_num
        self._hits_in_run += hit_condition
        return self._hits_in_run


class HitFrames(Imager):    
    def __init__(self, source, *args, gallery_len=3, **kwargs):
        self._hits_frame_buffer = deque(maxlen=gallery_len)
        super().__init__(source, *args, **kwargs)
        # Circular buffer for hit gallery.

    def last_frame(self, data:'{prefix}bg_subtracted', hit:'POINTING/hit_condition'):
        if hit:
            return data
    
    def frame_gallery(self, frame: '{prefix}last_frame'):
        self._hits_frame_buffer.append(frame.copy())
        missing_hits = self._hits_frame_buffer.maxlen \
            - len(self._hits_frame_buffer)  
        if missing_hits > 0:
            # Pad empty frames.
            empty_frame = np.zeros_like(self._hits_frame_buffer[0])
            for _ in range(missing_hits):
                self._hits_frame_buffer.append(empty_frame)
        return np.concatenate(self._hits_frame_buffer)
    

#cam1 = JetPosition(cam1_name, prefix='OAP/')
#cam2 = JetPosition(cam2_name, prefix='TSO/')
#pointing = BeamPointing(prefix='POINTING/')

##################################
### ADD instances of `HitFrames` here
### This will add a short gallery of previous "hits" for specific diagnostics
##################################

#HitFrames(cam3_name, prefix='Beam_monitor/')
