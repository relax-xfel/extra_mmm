import os
import numpy as np
import scipy
from scipy import optimize, ndimage
import matplotlib.pyplot as plt
import sys
import mmmUtils as mu
from PIL import Image
from importlib import reload 
import extra_mmm as em
import extra_data
import rossendorfer_farbenliste as rofl
from matplotlib import colors
from matplotlib.ticker import FormatStrFormatter
from datetime import datetime

lineoutDir='./lineouts/'
shotDir='./shots/'  #Directory to store output images
outDir='./overviews/'  #Directory to store output images


from metadata_client import MetadataClient
badshots=[]

def correct_delay(runNo,delay):
        #delay_c=delay+2.34 # this was working on 25. until evening
    #delay_c=delay+1034.986 for optical delay
    delay_c=delay+4279.3003
    if runNo>=120: 
        delay_c=delay_c+1.3
    if runNo>=129: 
        delay_c=delay_c+1.3+0.3
    if runNo>=219: 
        delay_c=delay_c+-3.4

    if runNo>=270: 
        delay_c=delay_c+0.5  #together  4279.3003
    if runNo>=353: 
        delay_c=delay_c+0.97

    if runNo>=532:
        delay_c=delay_c+0.5
        
    delay_c=np.round(delay_c*100)/100
    delay_c=delay_c*-1 #to satisfy Alejandro's condition
    return delay_c
        

def zyla_make(runNo,proposal=4446,darkrun=249,usepreshot=1,preshotoffset=-1):
    a=1000
    b=1500
    c=1000
    d=1600    

    a=700 # bigger raNGES
    b=1800
    c=700
    d=1900    
    run_dark = em.open_run(proposal, darkrun, data='all')  
    zyla_dark=em.get_image(run_dark,'Zyla')
    zyla_dark=zyla_dark[a:b,c:d]

    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
    zyla=em.get_image(run,'Zyla',PPU=PPU_open)           
    zyla=zyla[a:b,c:d]
    zyla=zyla-zyla_dark

    if usepreshot:
        run_pre = em.open_run(proposal, runNo+preshotoffset, data='all')  
        PPU_pre,PPU_trains,PPU_open_bool=em.get_PPU_open(run_pre,1)
        zyla_pre=em.get_image(run_pre,'Zyla',PPU=PPU_pre)
        zyla_pre=zyla_pre[a:b,c:d]
#        zyla_pre=mu.smooth2d(zyla_pre,5)
        #zyla_pre=zyla_pre-np.min(zyla_pre)
        zyla_pre=zyla_pre-zyla_dark
        zyla=zyla/zyla_pre
        zyla[np.logical_not(np.isfinite(zyla))]=0
    return zyla
    
def hapg_make(runNo,proposal=0,full=0):
    run = em.open_run(proposal, runNo, data='all')  
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)    
    jfB=em.get_image(run,'JF1',PPU=PPU_open)
    if np.size(jfB)>1:
        centralY=31
        spectraHeight=4
        HapgB_x0=1450
        HapgB_E0=8047
        HapgB_dispersion=-0.289/1.095*1.13
        jf1cut=jfB[centralY-spectraHeight:centralY+spectraHeight]
        spectrum_HapgB=np.nanmean(jf1cut,axis=0)
        spectrum_HapgBs=mu.smooth(spectrum_HapgB,7)
        spectrum_HapgBs[spectrum_HapgBs<0]=0
        
        if full:
            HapgB_x0=1450
            HapgB_E0=8047
            HapgB_dispersion=-0.289/1.095*1.13
            HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)*1e-3 -0.012
            return HapgB_xax,spectrum_HapgBs
        else:
            #silly processing
            spec=np.log10(spectrum_HapgBs)
            spec=np.flip(spec)
            spec=spec[1200:3000]
            return spec
    else:
        return 0 
    
def init_history():
    history=np.zeros((2000,5))
    mu.dumpPickle(history,'history')
    
    
def write_history(runNo,delay_c,resonance_inte,spatial_offset):
    history=mu.loadPickle('./history')
    history[runNo,:]=[delay_c,resonance_inte,spatial_offset,0,0]
    mu.dumpPickle(history,'history')

    
def draw_history(firstRun,currentRun):
    proposal=3446
    title='last'

    cmap=plt.cm.get_cmap("jet")
    history=mu.loadPickle('./history')
    mindel=0
    maxdel=0
    for runNo in np.arange(firstRun,800):
        
        row=history[runNo,:]
        delay=row[0]
        resonance=row[1]
        if resonance==0:continue
        lab=''
        col='g'
        symbol='o'
        if runNo in badshots:
            lab=''
            col=[0.5]
        if runNo==currentRun:
            lab=''
            col='r'
            
            
        if runNo<631:   # any shots before this one will be shown with tiny light marker, and without the text label    ************************
            col=[0.7,0.7,1]
            symbol='.'
            
            
        else:
            plt.text(delay,resonance+100,"{:.0f}".format(runNo),fontsize=8)
        plt.plot([delay],[resonance],marker=symbol,ms=7,color=col,label=lab)
        if delay<mindel:mindel=delay
        if delay>maxdel:maxdel=delay
    #plt.xlim(20,-20)
    plt.xlim(maxdel*1.1,mindel*1.1)
    
    plt.xlabel('Delay [ps]')
    plt.ylabel('Resonance')
#    plt.xlim(mintime, maxtime)
 #   plt.ylim(10e-3,2)        
    plt.title(title)
  #  plt.legend()
    plt.grid()
    yl=plt.ylim()
    yl=[0,yl[1]]
    plt.ylim(yl)
    #return intes

def get_metadata(runNo,debug=0):
        # Necessary configuration variables to establish a connection
    # Go to https://in.xfel.eu/metadata/oauth/applications to make a token for
    # the metadata catalogue.
    try:
        user_id = 'Mth7jWXuFt8BLlRTH56Hcd1YoVFZ6upm1a2o7yCBgI0'
        user_secret = 'CUsEZe4m-IfRyahjFqRZEO2FtvSyKYKJmyHFLXgyjEQ'
        user_id='xfQ3le8ImnoC5-v92DqqK_Eq2wEN8UdqYYwiUj5qxoU'
        user_secret='ETHQcCs3pQTvSyvtiCr_tNmDJ_y-D3gjlI7bzvb6wUY'
        user_email = 'm.smid@hzdr.de'
        #
        metadata_web_app_url = 'https://in.xfel.eu/metadata'
        token_url = 'https://in.xfel.eu/metadata/oauth/token'
        refresh_url = 'https://in.xfel.eu/metadata/oauth/token'
        auth_url = 'https://in.xfel.eu/metadata/oauth/authorize'
        scope = 'jupyter'
        base_api_url = 'https://in.xfel.eu/metadata/api/'

        # Generate the connection (example with minimum parameter options)
        client_conn = MetadataClient(client_id=user_id,
                                     client_secret=user_secret,
                                     user_email=user_email,
                                     token_url=token_url,
                                     refresh_url=refresh_url,
                                     auth_url=auth_url,
                                     scope=scope,
                                     base_api_url=base_api_url)
        runtypes={}
        runtypes[3837]='TestDAQ'
        runtypes[3926]='Calibration'
        runtypes[3287]='edge scan'
        runtypes[3288]='JF0'
        runtypes[3289]='JF1'
        runtypes[3290]='JF2'
        runtypes[3930]='main'
        runtypes[3928]='Cu foil'
        runtypes[3927]='Co foil'
        runtypes[3929]='Ni foil'

        samples={}
        samples[4275]='generic'


        proposal_number=4446
        #runNo=376
        resp = client_conn.get_proposal_runs(proposal_number,page=runNo,page_size=1)
        if np.size(resp['data']['runs'])==0:
            print('Run not found in metadata')
            return 0,0,0
        run=resp['data']['runs'][0]
        if debug: print (run)
        if run['experiment_id'] in runtypes:
            rtype=runtypes[run['experiment_id']]
        else:
            rtype="{:}".format(run['experiment_id'])
        
        if run['sample_id'] in samples:
            sample=samples[run['sample_id']]
        else:
            sample="{:}".format(run['sample_id'])

        from datetime import datetime
        te=run['end_at'][11:19]
        ts=run['begin_at'][11:19]
        end=datetime.strptime(te,'%H:%M:%S')
        begin=datetime.strptime(ts,'%H:%M:%S')
        duration=end-begin
        durs=str(duration)[2:]
        times=ts+' + '+durs
        return rtype, sample,times
    except Exception as e:
        print('Metadata reading failed:')
        print(str(e))
        return '?','?','?'


def overview_figure_A(proposal,runNo,mainshot_only=0,debug=0,generator='', ticking=0,recalc=0,train_selection='JP',parallel=1,reload_jet_positions=0):
    runtype,sample,times=get_metadata(runNo)
    if mainshot_only:
        if runtype!='main': 
            print('Run #{:.0f} is not a main shot.'.format(runNo))
            return
    mu.clear_times()
    if ticking: mu.tick('overview start')
            
    print('Going to do run #{:.0f}'.format(runNo))
    facecolor=[0.96,0.93,0.93]
    plt.figure(figsize=(18,10),facecolor=facecolor)
    r=5
    c=8
    
#    axZyla_pre = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axZyla_pre=[]
    axZyla=[]
    axZyla_ratio = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axHistory = plt.subplot2grid((r, c), (3, 4), colspan=4, rowspan=2)
    axHistory=[]
#    axZyla_post = plt.subplot2grid((r, c), (3, 2), colspan=1, rowspan=1)
 #   axZyla_profile = plt.subplot2grid((r, c), (3, 3), colspan=2, rowspan=2)

    axJF2 = plt.subplot2grid((r, c), (0, 2), colspan=2, rowspan=2)
    axJF2=[]
    
    axTrains = plt.subplot2grid((r, c), (0, 2), colspan=2, rowspan=1)
    axPointing=plt.subplot2grid((r, c), (1, 2), colspan=2, rowspan=2)

    #axTest = plt.subplot2grid((r, c), (0, 2), colspan=1, rowspan=1)
 #   axSaxs_back = plt.subplot2grid((r, c), (0, 5), colspan=1, rowspan=1)
#    axHello = plt.subplot2grid((r, c), (0, 4), colspan=1, rowspan=1)
    
  #  axSaxs_pre = plt.subplot2grid((r, c), (1, 2), colspan=2, rowspan=1)
   # axSaxs_rot = plt.subplot2grid((r, c), (2, 2), colspan=2, rowspan=1)
    #axSaxs_profile = plt.subplot2grid((r, c), (2, 6), colspan=2, rowspan=2)
    
    axSpectra = plt.subplot2grid((r, c), (2, 1), colspan=1, rowspan=1)
    axInfo = plt.subplot2grid((r, c), (0, 0), colspan=1, rowspan=2)
 #   axPCI_profile = plt.subplot2grid((r, c), (4, 6), colspan=2, rowspan=1)


    axC1 = plt.subplot2grid((r, c), (3, 0), colspan=2, rowspan=2)
    axC2 = plt.subplot2grid((r, c), (3, 2), colspan=2, rowspan=2)
    axC3 = plt.subplot2grid((r, c), (3, 4), colspan=2, rowspan=2)

    axOP = plt.subplot2grid((r, c), (0, 4), colspan=2, rowspan=2)
    axP1 = plt.subplot2grid((r, c), (0, 6), colspan=2, rowspan=2)
    axP2 = plt.subplot2grid((r, c), (2, 6), colspan=2, rowspan=2)
    
    axRelax1=plt.subplot2grid((r, c), (2, 0), colspan=1, rowspan=1)
    axSpectra=plt.subplot2grid((r, c), (2, 4), colspan=2, rowspan=1)
    axHapgBImage=plt.subplot2grid((r, c), (2, 6), colspan=2, rowspan=1)
    axJF2=plt.subplot2grid((r, c), (3, 6), colspan=2, rowspan=1)
    axJF4=plt.subplot2grid((r, c), (4, 6), colspan=2, rowspan=1)

    col_Ge=rofl.g()
    col_HapgA=rofl.b()
    col_HapgB=rofl.o()    
    run = em.open_run(proposal, runNo, data='all')  #Opening the given run
    if run==0: return 0
    #epix=em.get_image(run,'ePix1',trainId,debug=debug,recalc=recalc)
    #trainId,shot_ratio=em.get_shot_trainId_IPM(run,debug=debug)            
    PPU_open,PPU_trains,PPU_open_bool=em.get_PPU_open(run,1)
#    if 'HED_IA1_JF500K4/DET/JNGFR01:daqOutput' in run.all_sources:
    #trainId,shot_ratio,JF4_sum,JF4_trains=em.get_shot_trainId_3(run,debug=0,diag='JF1')        
   # trainIdaa,shot_ratioaa,CAM3_sum,CAM3_trains=em.get_shot_trainId_3(run,debug=0,diag='CAM3',threshold=0)
    #shot=np.size(PPU_open)==1
    #shot=(np.size(PPU_open)>=1)*(np.size(PPU_open)<=2)
    if len(train_selection)>2:
        if train_selection[:2]=='JP':
            if not parallel:
                jet_positions=em.get_jet_positions(run,debug=0,reload=reload_jet_positions)
            else:
                jet_positions=em.get_jet_positions_parallel(run,debug=0,reload=reload_jet_positions)
            good=em.get_jet_pointing_good_trains(run,train_selection=train_selection)
            good_jets=np.size(good)
            total_jets=np.shape(jet_positions)[0]
            print('good jets: {:}'.format(good_jets))
            print('all jets: {:}'.format(total_jets))
            if good_jets==0:
                print('There are no good shots!')
                return 0,[],[]
                    
                    
    shot=0

    text2='runtype: {:s}, sample: {:s}'.format(runtype,sample)
    plt.gcf().text(0.1,0.01, text2, fontsize=16,color=[0.4,0.1,0.1])
    
    postshot=(runtype=='post')
    preshot=(runtype=='pre')
    if ticking: mu.tick('info')
    if axInfo!=[]:
        plt.sca(axInfo)

        plt.axis('off')
        em.print_text_rw=0.025
        em.print_text_y=0.9
        em.print_text_row=0
        plt.gcf().text(0.01, 0.95, "p{:04.0f}".format(proposal),color=[0,0.2,0.5],fontsize=24)
        plt.gcf().text(0.1, 0.95, "#{:03.0f}".format(runNo),color=[0.5,0.2,0.],fontsize=24)
        now = datetime.now()
        current_time = now.strftime("%H:%M")
        #plt.gcf().text(0.02, 0.92, "trainId {:.0f} , ratio {:.2e}, overview time: {:s}".format(trainId,shot_ratio,current_time))
        plt.gcf().text(0.01, 0.92, "Time: {:s}".format(times),fontsize=16)
        plt.gcf().text(0.18, 0.92, "Overview time: {:s}  {:s}".format(current_time,generator),fontsize=8)
        
#        if shot:plt.gcf().text(0.18, 0.95, "shot!",color='w',fontsize=18,backgroundcolor=[0.9,0.7,0.])
 #       elif postshot: plt.gcf().text(0.18, 0.95, "postshot",color=[0.2,0.2,0.7],fontsize=18)
  #      elif preshot: plt.gcf().text(0.18, 0.95, "preshot",color=[0.2,0.2,0.7],fontsize=18)
   #     else:plt.gcf().text(0.18, 0.95, "accumulation",color=[0.0,0.4,0.8],fontsize=18)
        plt.gcf().text(0.18, 0.95, train_selection,color=[0.0,0.4,0.8],fontsize=18)
        
        xgm_sa2,tr=em.get_array(run,'XGM_SA2',train_selection=train_selection,debug=debug)
        att_sa2,tr=em.get_array(run,'ATT_SA2',train_selection=train_selection,recalc=recalc)        
        xgm_hed,tr=em.get_array(run,'XGM_HED',train_selection=train_selection,recalc=recalc)
        att_hed,tr=em.get_array(run,'ATT_HED',train_selection=train_selection,recalc=recalc)
        IPMcalib='3430'
        ipm_opt,tr=em.get_IPM_OPT(run,train_selection=train_selection,debug=0,calibration=IPMcalib)
#        delay,tr=em.get_array(run,'delay',train_selection=train_selection)
        delay,tr=em.get_array(run,'phaseshifter',train_selection=train_selection)
        delay_c=correct_delay(runNo,delay)
        em.print_text("XGM SA2 {:s} μJ",xgm_sa2)
        em.print_text("ATT_SA2: {:s} %",att_sa2*100)
        em.print_text("XGM_HED: {:s} μJ",xgm_hed)
        em.print_text("PPU: open {:s} x",np.size(PPU_open))
        #em.print_text("Train selection: {:}".format(train_selection),0)
        
        em.print_text("ATT_HED: {:s} %",att_hed*100)
        em.print_text("IPM_OPT: {:s} μJ.",ipm_opt)
        transmission1=xgm_hed/xgm_sa2/att_sa2
        em.print_text("trans. betw. XGMs: {:s}%.",transmission1*1e2,derived=1)

        beamline_trans2=ipm_opt/xgm_sa2/att_sa2/att_hed
        em.print_text("trans. till IPM: {:s}%.",beamline_trans2*1e2,derived=1)
        
   #     scanx,tr=em.get_array(run,'SCAN_X',PPU=PPU_open,trainId=trainId)
  #      scany,tr=em.get_array(run,'SCAN_Y',PPU=PPU_open,trainId=trainId)
 #       em.print_text("FSS X: {:s}",scanx,derived=0)
#        em.print_text("FSS Y: {:s}",scanx,derived=0)
        motors=['SCAN_X','SCAN_Y','OAP_X','OAP_Y','OAP_Z']
        for motor in motors:
            mot,tr=em.get_array(run,motor,train_selection=train_selection)
            em.print_text("{:s} {:s}".format(motor,'{:s}'),mot,derived=0)
            
        row=16
        #if shot:
        if 0:
            if np.size(delay)==0:
                pos=text.find('{')
                textnull='no delay'
                col='red'
                plt.gcf().text(0.02,emprint_text_y-em.print_text_rw*row, textnull, fontsize=24,color=col)
            else:
                text2="delay: {:.1f} ps".format(delay_c)
                col='blue'
                plt.gcf().text(0.02,em.print_text_y-em.print_text_rw*row, text2, fontsize=24,color=col)

                
# TRAINS
    if axTrains!=[]:
        plt.sca(axTrains)
        #trains of pointing:
        maxtr=1
        tr0=0
        if len(train_selection)>2:
            if train_selection[:2]=='JP':
                trs=jet_positions[:,0]
                tr0=np.min(trs)
                trs=trs-tr0
                sel,dist=em.get_jet_pointing_good_trains(run,return_distance=1,train_selection=train_selection)
                plt.plot(trs,dist,'k.',label='jet offset')
                maxtr=np.nanmax(dist)
                plt.plot(trs[sel],dist[sel],'r.')
            mu.plt_zero_origin()
            plt.title('Trains')

        if 1:
            val,CAM3_trains=em.get_array(run,'CAM3',train_selection='all')  #reading the data
            dims=np.arange(1,np.size(np.shape(val))) #which dimensions to sum over
            CAM3_sum=np.nansum(val,axis=tuple(dims))
    
            CAM3_sum=CAM3_sum/np.nanmax(CAM3_sum)*maxtr
            l=plt.plot(CAM3_trains-tr0,CAM3_sum,'bx',label='CAM3',zorder=-20,ms=3,alpha=0.5)
            plt.legend()
        ipm_opt_all,tr=em.get_IPM_OPT(run,train_selection='all',debug=0,calibration=IPMcalib)
        if np.size(ipm_opt_all)>1:
            smi=mu.smooth(ipm_opt_all,21)
            zerotrain=tr[0]
            trm=tr-zerotrain
            maxtr=np.nanmax(ipm_opt_all)
            l=plt.plot(trm,ipm_opt_all,'k.',label='IPM')
            plt.ylabel('IPM OPT [~μJ]')
            axTrains.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
            plt.plot(trm,smi,'-',lw=20,zorder=-10,color=[0,0.2,0.8,0.2])
            trnum=np.size(tr)
            ax=plt.gca()
            plt.xlim(0,np.max(trm))
            plt.text(0.3,0.25,'{:.0f} trains total'.format(trnum),transform = ax.transAxes,color='red')
            plt.text(0.3,0.15,'first train:{:.0f}'.format(tr[0]),transform = ax.transAxes,color='red')
    
            yl=plt.ylim()               
            PPU_open_bool=PPU_open_bool*1.
            PPU_open_bool[PPU_open_bool==0]=np.nan            
            plt.plot(PPU_trains-zerotrain,PPU_open_bool*yl[1],'b*',label='PPU')
            yl=np.array([yl[0],yl[1]*1.02])
            plt.ylim(yl)
            
            plt.legend()
            
    if np.isnan(ipm_opt): ipm_opt=1

    # Pointing
    if axPointing!=[]:
        plt.sca(axPointing)
        if len(train_selection)>2:
            if train_selection[:2]=='JP':
                jet_positions=em.get_jet_positions_parallel(run,debug=0,reload=0)
                plt.plot([0],[0],'bo')
                plt.plot(jet_positions[:,1],jet_positions[:,2],'ko',alpha=0.3)
                sel=em.get_jet_pointing_good_trains(run,return_bool_array=1,train_selection=train_selection)
                plt.plot(jet_positions[sel,1],jet_positions[sel,2],'ro',alpha=0.3)
                hitnum=np.sum(sel)
                jetnum=np.shape(jet_positions)[0]
            else:
                jetnum=hitnum=np.nan
            plt.grid()
            plt.title('Jet Pointing')
            plt.ylabel('position in CAM2 (TSO)')
            plt.xlabel('position in CAM1 (OAP)')
            plt.title('Jet Pointing ({:} hits/{:} jets)'.format(hitnum,jetnum))

        
    if axRelax1!=[]:
        img=em.get_image(run,'Relax_NF',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axRelax1)
            plt.imshow(img, cmap=rofl.cmap())
            plt.clim(np.quantile(img,[0.1,0.99]))
            plt.colorbar()
            plt.title('Relax NF')
            
    if axOP!=[]:
        img=em.get_image(run,'OP',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axOP)
            plt.imshow(img, cmap=rofl.cmap())
            plt.clim(np.quantile(img,[0.1,0.99]))
            plt.colorbar()
            plt.title('Optique-Peter')

    if axP1!=[]:
        img=em.get_image(run,'FDI2',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axP1)
            plt.imshow(img, cmap=rofl.cmap())
#            plt.clim(500,3000)
 #           plt.xlim(700,1300)
      #      plt.ylim(500,1500)
            plt.clim(np.quantile(img,[0.1,0.9]))
            plt.colorbar()
            plt.title('Proton profiler')
    if axP2!=[]:
        img=em.get_image(run,'LT2',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axP2)
            plt.imshow(img, cmap=rofl.cmap())
            plt.clim(np.quantile(img,[0.1,0.9]))
            plt.colorbar()
            plt.title('Proton spectrometer')

    if shot:
        run_pre = em.open_run(proposal, runNo-1, data='all')  #Opening the given run
    # %% HAPG 
    jfB=em.get_image(run,'JF1',train_selection=train_selection,debug=debug,recalc=recalc,threshold_lower=6)
    if np.size(jfB)>1:
        centralY=31
        cut_left=0
        spectraHeight=6
        jfB2=jfB
        imH=np.shape(jfB2)[0]
        imW=np.shape(jfB2)[1]
        jf1cut=jfB2[centralY-spectraHeight:centralY+spectraHeight]
        HapgB_x0=0
        HapgB_dispersion=1
        HapgB_E0=0
        if 1:
            spectrum_HapgB=np.nanmean(jf1cut,axis=0)
            spectrum_HapgBs=mu.smooth(spectrum_HapgB,15)
            spectrum_HapgBs[spectrum_HapgBs<0]=0
            #spectrum_HapgBsn=spectrum_HapgBs/np.max(spectrum_HapgBs)
            norma=np.mean(spectrum_HapgBs[1300:1500])*2
            spectrum_HapgBsn=spectrum_HapgBs/norma
            HapgB_xax=((np.arange(np.size(spectrum_HapgB))-HapgB_x0)*HapgB_dispersion + HapgB_E0)
#            mu.dumpPickle([HapgB_xax,spectrum_HapgB],lineoutDir+'{:04.0f}_HapgB'.format(runNo))

        if axHapgBImage!=[]:
            plt.sca(axHapgBImage)
            if np.size(jfB2)>1 and np.nansum(jfB2)>0:
                plt.imshow(jfB2, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
                mu.draw_horizontal_edges(centralY,spectraHeight,color='w',x1=3000)
                mm=np.quantile(jfB2,0.999)
                if np.isfinite(mm) and mm>0:
                    plt.clim(mm*1e-3,mm)
                    plt.colorbar()

#            plt.xlim(0,2200)
            plt.ylim(0,60)    
            plt.title('JF1: Spectrometer')
            if axSpectra!=[]:
                plt.sca(axSpectra)
                plt.semilogy(spectrum_HapgBsn,'r',label='JF1')
                plt.title('Spectra')
                plt.legend()
                if 0:
                    plt.semilogy(HapgB_xax,spectrum_HapgBs,label='Backward')
                    selback=(HapgB_xax>7.9)*(HapgB_xax<7.95)
                    back=np.mean(spectrum_HapgBs[selback])
                    if np.max(spectrum_HapgBs)>1e2:
                        plt.ylim(1e2,1e5)
                    plt.vlines([8.047,8.027,8.15],0,1e5,'k',zorder=-20)
                    plt.xlim(7.9,8.4)
                    plt.legend()
                    # %% measuring resonance peak
                    minE=8.13
                    maxE=8.17
                    mu.plot_vline_till_plot([minE,maxE],HapgB_xax,spectrum_HapgBs,y0=0,color='g', linewidth=1)
                    plt.plot([minE,maxE],[back,back],'g-',linewidth=1)
            #        mu.plot_vline_till_plot(maxE,HapgB_xax,spectrum_HapgBs,y0=0,color='g', linewidth=1)

                    specback=spectrum_HapgBs-back
                    selE=(HapgB_xax>minE)*(HapgB_xax<maxE)
                    resonance_inte=np.sum(specback[selE])
                    plt.text(8.2,1e4,'{:.0f}'.format(resonance_inte),color='g')
                    if 0: # debug hapg make
                        spec=hapg_make(runNo)
                        plt.semilogy(HapgB_xax,spec,label='test')

        
    if axJF2!=[]:
        img=em.get_image(run,'JF2',train_selection=train_selection,debug=debug,recalc=recalc,threshold_lower=6)           
        plt.sca(axJF2)
        if np.size(img)>1 and np.nansum(img)>0:
            plt.imshow(img, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
            mm=np.quantile(img,0.99999)
            if np.isfinite(mm) and mm>0:
                plt.clim(mm*1e-3,mm)
                plt.colorbar()
            plt.title('JF2')
            plt.ylim(400,512)
            plt.ylim(350,450)
            
###spectrra
            centralY=400
            cut_left=0
            spectraHeight=20
            jfcut=img[centralY-spectraHeight:centralY+spectraHeight]
            Hapg2_x0=0
            Hapg2_dispersion=4
            Hapg2_E0=0
            spectrum_Hapg2=np.nanmean(jfcut,axis=0)
            spectrum_Hapg2s=mu.smooth(spectrum_Hapg2,15)
            spectrum_Hapg2s[spectrum_Hapg2s<0]=0
            spectrum_Hapg2s[:50]=0
            spectrum_Hapg2sn=spectrum_Hapg2s/np.max(spectrum_Hapg2s)
            Hapg2_xax=((np.arange(np.size(spectrum_Hapg2))-Hapg2_x0)*Hapg2_dispersion + Hapg2_E0)                   
            mu.draw_horizontal_edges(centralY,spectraHeight,color='w',x1=1024)
            if axSpectra!=[]:
                plt.sca(axSpectra)
                plt.semilogy(Hapg2_xax,spectrum_Hapg2sn,'g',label='JF2')
                plt.ylim(1e-4,1)
                plt.legend()

##
    if axJF4!=[]:
        img=em.get_image(run,'JF4',train_selection=train_selection,debug=debug,recalc=recalc,threshold_lower=6)           
        plt.sca(axJF4)
        if np.size(img)>1:
            img=mu.smooth2d(img,3)
            if np.nansum(img)>0:
                plt.imshow(img, cmap=rofl.cmap(),aspect='auto',norm=colors.LogNorm())
                mm=np.quantile(img,0.999)
                if np.isfinite(mm) and mm>0:
                    plt.clim(mm*1e-5,mm)            
                    plt.colorbar()
            plt.title('JF4 (SAXS)')            
            plt.xlim(600,1024)
            plt.ylim(0,350)
    
    #zyla PCI
    zmain='r'
    zpre='b'
    zpost='k'
    if axZyla!=[]:
        if ticking: mu.tick('PCI')
        zyla=em.get_image(run,'Zyla',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(zyla)>1:
            zylab=zyla[250:2000,500:2000]
            zylab=mu.smooth2d(zylab,11)
            if np.nansum(zylab)>0:
                zr=np.reshape(zylab,(1,np.size(zylab)))
                a,b=np.histogram(zr,np.arange(90,200,0.1))
                am=np.argmax(a)
                zyla_back=b[am]
                zyla_max=np.max(zyla)
                zyla=zyla-zyla_back
                if preshot:
                    plt.sca(axZyla_pre)
     #           elif postshot:
    #                plt.sca(axZyla_post)
                else:
                    plt.sca(axZyla)
                zyla_int=np.sum(zyla)*1e-10/0.0021/0.0007/0.77
                em.print_text("E deposited on ZYLA {:s} a.u.",zyla_int,13,derived=0)            
                em.print_text(" - ratio to IPM: {:s}",zyla_int/ipm_opt,14,derived=0)
    #            em.print_text(" - max count: {:s} ",zyla_max,15,derived=0)
                plt.imshow(zyla, cmap=rofl.cmap())
                mz=(np.max(zylab)-zyla_back)
                maxs=np.array([0.1,0.5,1,3,10,30,100,300,1000,3000])
                am=np.argmin(np.abs(mz-maxs))
                maxset=maxs[am]
            plt.clim(0,mz*1)
            plt.xlim(500,1500)
            plt.ylim(500,1500)
            plt.title('Zyla, shot')

    if ticking: mu.tick('PCI -preshot ')          

    if 0:
        plt.sca(axHello)
        import matplotlib.image as mpimg
        if shot:
            if 0:
                if np.mean(spectrum_HapgBs)>150:
                    img = mpimg.imread('./files/hello2.png')                
                else:
                    img = mpimg.imread('./files/bye2.png')
            else:
                img = mpimg.imread('./files/hello.png')
        elif postshot:
            img = mpimg.imread('./files/bye.png')                
        elif preshot:
            img = mpimg.imread('./files/preshot.png')
        else:
            img = mpimg.imread('./files/hellono.png')
            
        plt.imshow(img)
        plt.axis('off')
    
    spatial_offset=0
    if shot:
        write_history(runNo,delay_c,resonance_inte,spatial_offset)

    if axHistory!=[]:
        plt.sca(axHistory)
        draw_history(537,runNo)

    
#The new things for 4446
    if axC1!=[]:
        img=em.get_image(run,'CAM1',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axC1)
            plt.imshow(img, cmap='gray')
            plt.clim(300,2000)
            plt.xlim(600,1200)
            plt.ylim(800,4000)
            plt.colorbar()
            plt.title('CAM1 (OAP)')
        
    if axC2!=[]:
        img=em.get_image(run,'CAM2',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axC2)
            plt.imshow(img, cmap='gray')
            plt.clim(0,2000)
            plt.xlim(1500,2500)
            plt.colorbar()
            plt.ylim(0,2048)
            plt.title('CAM2 (TSO)')

    if axC3!=[]:
        img=em.get_image(run,'CAM3',train_selection=train_selection,debug=debug,recalc=recalc)           
        if np.size(img)>1:
            plt.sca(axC3)
            img=img[250:1000,800:1700]
            plt.imshow(img, cmap=rofl.cmap())
            
            plt.colorbar()
            CAM3_sum=np.sum(img)
            plt.clim(0,1000)
            
            plt.clim(0,np.quantile(img,[0.99]))
            plt.title('CAM3 (sum:{:.2e})'.format(CAM3_sum))
                                
    
    plt.tight_layout()        
    plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}_{:s}_{:}.jpg'.format(proposal,runNo,generator,train_selection),dpi=200,facecolor=facecolor)
    if generator.find('o')>=0:
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)    
    if shot:
        plt.savefig(shotDir+'Shot_r{:05.0f}.jpg'.format(runNo),dpi=200,facecolor=facecolor)
        plt.savefig(shotDir+'Shot_last.jpg'.format(delay_c*-100+100000,runNo),dpi=200,facecolor=facecolor)
        
    if 0:
        plt.figure(figsize=(18,10),facecolor=facecolor)
        overview_figure_A(proposal,runNo-1,1,add_postshot=1)# now do the main shot including picutre of this postshot
        plt.savefig(outDir+'Run_p{:04.0f}_r{:04.0f}.jpg'.format(proposal,runNo-1),dpi=200,facecolor=facecolor)
        plt.savefig(outDir+'Run_p{:04.0f}_last.jpg'.format(proposal,runNo),dpi=200,facecolor=facecolor)    

    #plt.show()
    mu.print_times()
    return CAM3_sum,spectrum_HapgBsn,spectrum_Hapg2sn
    #return 'Ok'

