# extra_mmm

Library to ease near online data analysis at XFEL.  Mostly a wrapper for extra_data.

Do see the [Wiki](https://codebase.helmholtz.cloud/relax-xfel/extra_mmm/-/wikis/home)
