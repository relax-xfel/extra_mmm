## set of useful functions
## summarized my M.Smid, based on some older work by many colleagues
## especially, Thomas Preston, Oliver Humphires, Thomas Kluge

import glob
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as sc
import scipy.signal as ss
from PIL import Image
import os
from scipy.signal import savgol_filter
from scipy import ndimage, misc, optimize
import extra_data
import mmmUtils as mu
import time
import xarray as xr
from tqdm import tqdm

import metro_context_em as mc


global scratchDir
scratchDir="./data"
global use_sftp
global sftp_connection

use_sftp=0
sftp_connection=0

global local
global local_run_info
diags=[]
global online_proc
online_proc=-1

diags.append(['JF1',"HED_IA1_JF500K1/DET/JNGFR01:daqOutput",0,'data.adc'])
diags.append(['JF2',"HED_IA1_JF500K2/DET/JNGFR02:daqOutput",0,'data.adc'])
diags.append(['JF3',"HED_IA1_JF500K3/DET/JNGFR03:daqOutput",0,'data.adc'])
diags.append(['JF4',"HED_IA1_JF500K4/DET/JNGFR04:daqOutput",0,'data.adc'])
diags.append(['ePix1',"HED_IA1_EPX100-1/DET/RECEIVER:daqOutput",0,'data.image.pixels'])
diags.append(['ePix2',"HED_IA1_EPX100-2/DET/RECEIVER:daqOutput",0,'data.image.pixels'])
#diags.append(['Zyla',"HED_EXP_ZYLA/CAM/5:daqOutput",0,'data.image.pixels']) This is actually Optique-Peter!
diags.append(['Electrons',"HED_IA1_LT/CAM/CAM_3:daqOutput",0])
diags.append(['HIDG1',"HED_EXP_HIDG/CAM/CAM1:daqOutput",0])
diags.append(['HIDG2',"HED_EXP_HIDG/CAM/CAM2:daqOutput",0])
diags.append(['HIDG3',"HED_EXP_HIDG/CAM/CAM3:daqOutput",0])
#diags.append(['ILM',"HED_IA1_LT/CAM/CAM_2:daqOutput",0])
diags.append(['CRL_Z',"HED_IA1_NFS/MOTOR/CRL_Z",0])
diags.append(['XGM_HED',"HED_XTD6_XGM/XGM/DOOCS:output",0,'data.intensityTD'])
diags.append(['XGM_SA2',"SA2_XTD1_XGM/XGM/DOOCS:output",0,'data.intensityTD'])
diags.append(['LC_AXIS_4',"HED_IA1_DETM/MOTOR/LC_AXIS_4",0,"actualPosition.value"])
diags.append(['SCAN_Y',"HED_IA1_FSSS/MOTOR/SCAN_Y",0,"actualPosition.value"])
diags.append(['SCAN_Y_enc',"HED_IA1_FSSS/MOTOR/SCAN_Y",0,"encoderPosition.value"])
diags.append(['SCAN_X',"HED_IA1_FSSS/MOTOR/SCAN_X",0,"actualPosition.value"])
diags.append(['ATT_SA2',"SA2_XTD1_ATT/MDL/MAIN",0,"actual.transmission.value"])
diags.append(['ATT_HED',"HED_OPT_ATT/MDL/ATT",0,"actual.transmission.value"])
diags.append(['delay',"HED_HPLAS_HIL/DOOCS/OPTICALDELAY",0,"actualPosition.value"])
diags.append(['phaseshifter',"HED_HPLAS_HIL/DOOCS/PHASESHIFTER",0,"actualPosition.value"])
diags.append(['phaseshifter_playground',"HED_EXP_SYS/MDL/SLOW_DATA_SELECT",0,"hedHplasHilDoocsPhaseshifter.actualPosition.value"])
diags.append(['delay_actual',"HED_EXP_SYS/MDL/SLOW_DATA_SELECT",0,'hedPlaygroundMdlRelaxsync.actualPosition.value'])

diags.append(['IPM_OPT_ch2',"HED_OPT_IPM/ADC/1:channel_2.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch3',"HED_OPT_IPM/ADC/1:channel_3.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch4',"HED_OPT_IPM/ADC/1:channel_4.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch5',"HED_OPT_IPM/ADC/1:channel_5.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch6',"HED_OPT_IPM/ADC/1:channel_6.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch7',"HED_OPT_IPM/ADC/1:channel_7.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch8',"HED_OPT_IPM/ADC/1:channel_8.output",0,'data.peaks'])
diags.append(['IPM_OPT_ch9',"HED_OPT_IPM/ADC/1:channel_9.output",0,'data.peaks'])

diags.append(['IPM_OGT_ch2',"HED_OGT_IPM/ADC/1:channel_2.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch3',"HED_OGT_IPM/ADC/1:channel_3.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch4',"HED_OGT_IPM/ADC/1:channel_4.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch5',"HED_OGT_IPM/ADC/1:channel_5.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch6',"HED_OGT_IPM/ADC/1:channel_6.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch7',"HED_OGT_IPM/ADC/1:channel_7.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch8',"HED_OGT_IPM/ADC/1:channel_8.output",0,'data.peaks'])
diags.append(['IPM_OGT_ch9',"HED_OGT_IPM/ADC/1:channel_9.output",0,'data.peaks'])

#added by Lisa for 3082
#diags.append(['FDI',"HED_EXP_ZYLA/CAM/42:daqOutput",0,'data.image.pixels'])
diags.append(['Pointing','HED_EXP_HIDG/CAM/CAM13:daqOutput',0,'data.image.pixels'])
diags.append(['PAM',"HED_OPT_PAM/CAM/ACA1920-1:daqOutput",0,'data.image.pixels'])

diags.append(['delay2',"HED_LAS_COM/MOTOR/DELAY_EXP",0,"actualPosition.value"])


diags.append(['OAP_Y',"HED_IA1_LT/MOTOR/OAP_Y",0,"actualPosition.value"])
diags.append(['OAP_X',"HED_IA1_LT/MOTOR/OAP_X",0,"actualPosition.value"])
diags.append(['OAP_Z',"HED_IA1_LT/MOTOR/OAP_Z",0,"actualPosition.value"])


#Added fro 4446 - Sebastian
#cam1_name = 'HED_EXP_PPT/CAM/CAM_1:daqOutput@HED_DAQ_DATA/DA/8:output'
#cam2_name = 'HED_IA1_LT/CAM/FSI:daqOutput@HED_DAQ_DATA/DA/8:output'
diags.append(['CAM1',"HED_EXP_PPT/CAM/CAM_1:daqOutput",0,'data.image.pixels'])
diags.append(['CAM2','HED_IA1_LT/CAM/FSI:daqOutput',0,'data.image.pixels'])
diags.append(['CAM3','HED_IA1_LT/CAM/CAM_3:daqOutput',0,'data.image.pixels'])
diags.append(['Fisheye','HED_IA1_IC/CAM/FISHEYE_VIEW:daqOutput',0,'data.image.pixels'])
diags.append(['FDI2','HED_EXP_FDI/CAM/ACA1920-2:daqOutput',0,'data.image.pixels'])
diags.append(['PPT2',"HED_EXP_PPT/CAM/CAM_2:daqOutput",0,'data.image.pixels'])
diags.append(['OP',"HED_EXP_ZYLA/CAM/5:daqOutput",0,'data.image.pixels'])
diags.append(['PAM_Zyla2','HED_OPT_PAM/CAM/ZYLA-2:daqOutput',0,'data.image.pixels'])
diags.append(['PAM_Zyla1','HED_OPT_PAM/CAM/ZYLA-1:daqOutput',0,'data.image.pixels'])

diags.append(['Relax_NF','HED_EXP_HIDG/CAM/CAM1:daqOutput',0,'data.image.pixels'])

diags.append(['LT1',"HED_IA1_LT/CAM/CAM_1:daqOutput",0])
diags.append(['LT2',"HED_IA1_LT/CAM/CAM_2:daqOutput",0])

diags.append(['FDI1','HED_EXP_FDI/CAM/ACA1920-1:daqOutput',0,'data.image.pixels'])






global ePix1_dark
ePix1_dark=[]





def version():
    ver="extra_mmm 7.9.2023"
    print(ver)
    return ver

def ImSave(data,name,debug=0):
    array = np.array(data)
    debug=0
    if debug:
        try:
            dummy = Image.fromarray(array)
            print(np.shape(dummy))
            print(type(dummy))
            dummy.save(name)
        except Exception as e: # work on python 3.x
            print(str(e))
            print("Some error while saving...")


    else:
        dummy = Image.fromarray(array)
        dummy.save(name)


def open_run(proposal, runNo, data='all',verbose=0,local_dir='',scratch='extra_mmm_cache'):
    global scratchDir
    global local
    if local_dir!='':
        scratchDir=local_dir
        local=1
        global local_run_info
        local_run_info=[runNo,proposal,data]
    else:
        local=0
        try:
            run = extra_data.open_run(proposal, runNo, data=data)  #Opening the given run
            filename=run.files[0].filename
    #        rawpos=np.max([filename.find('raw'),filename.find('proc')])
            rawpos=np.max([filename.find('raw'),filename.find('proc'),filename.find('usr')])
            basedir=filename[:rawpos]
#            scratchDir=basedir+'scratch/extra_mmm/'
 #           scratchDir=basedir+'usr/Shared/extra_mmm_cache/'
            scratchDir=basedir+'usr/Shared/'+scratch+'/'
            if verbose:
                print('##  Run {:.0f} succesfully opened (at least we hope so...).'.format(runNo))
                print('##  Scratch dir: {:s}'.format(scratchDir))
                print('Files:')
                for f in run.files:
                    print(f)
            mkdir(scratchDir,verbose)
            mu.grant_all(scratchDir)
 #           mkdir("./overviews",verbose)
#            mkdir("./lineouts",verbose)
            return run
        except Exception as e: # work on python 3.x
            print (e)
            if verbose:
                print('Run {:.0f} does not have data (yet).'.format(runNo))
            return 0

#I assume the run was already open by extra_data, I just want to prepare this library
def init_run(run,verbose=0,proc=0):
    global scratchDir
    global local
    local=0
    runNo,proposal,procW= get_run_info(run,debug=0)
    #the procw from get run info seems might be wrong. I assume it is proc=0 unless given here
    global online_proc
    online_proc=proc
    try:
        filename=run.files[0].filename
        rawpos=np.max([filename.find('raw'),filename.find('proc'),filename.find('usr')])
        basedir=filename[:rawpos]
        scratchDir=basedir+'scratch/extra_mmm/'
        scratchDir=basedir+'usr/Shared/extra_mmm_cache/'
        if verbose:
            print('##  Run {:.0f} succesfully opened (at least we hope so...).'.format(runNo))
            print('##  Scratch dir: {:s}'.format(scratchDir))
            print('Files:')
            for f in run.files:
                print(f)
        mkdir(scratchDir,verbose)
        mu.grant_all(scratchDir)
#        mkdir("./overviews",verbose)
 #       mkdir("./lineouts",verbose)
        return run
    except Exception as e: # work on python 3.x
        print (e)
        if verbose:
            print('Run {:.0f} does not have data (yet).'.format(runNo))
        return 0

def mkdir(fn,verbose=1):
    if not os.path.isdir(fn):
        if verbose:
            print("directory {:s} created.".format(fn))
        os.mkdir(fn)

def get_motor_values(run,nickname):
    motor=get_fullname(nickname)
    posMotor=run.get_array(motor,"actualPosition.value")
    return posMotor.values

def get_diag_trainIds(run,nickname):
    motor=get_fullname(nickname)
    if "actualPosition.value" in run.keys_for_source(motor):
        posMotor=run.get_array(motor,"actualPosition.value")
        trainIds = posMotor.coords['trainId']
    elif "data.trainId" in run.keys_for_source(motor):
        trains=run.get_array(motor,"data.trainId")
        trainIds = trains.values
    else:
        print('not sure where to take trainIds..')

    return trainIds

def get_shot_trainId_3(run,diag='JF4',debug=0,use_cache=0,threshold=5):
    """
    as get_shot_trainId_2,  but returns also the val_sum
    """
    #caching, to speed up things
    if use_cache:
        runNo,propNo,proc=get_run_info(run)
        ss='_trainId'
        if not proc:
            ss=ss+'_raw'
        fn=scratchDir+"/p{:04.0f}_r{:04.0f}_{:s}{:s}".format(propNo,runNo,diag,ss)
        pick=mu.loadPickle(fn)
        if pick!=0:
            if debug:
                print('shot trainId loaded from local cache.')
            return pick[0],pick[1]

    #val,tr=get_array_raw(run,diag)  #reading the data
    val,tr=get_array(run,diag,trainId=0)  #reading the data
    dims=np.arange(1,np.size(np.shape(val))) #which dimensions to sum over
    if threshold!=0:
        val[val<threshold]=0 # thresholding
    val_sum=np.nansum(val,axis=tuple(dims))
    if np.size(val_sum)==0:
        return 0,0,0
    val_sum_o=val_sum*1.0
    maxi=np.nanargmax(val_sum)  #index of strongest element
    maxv=val_sum[maxi]

    if val_sum.dtype=='uint64':
        val_sum[maxi]=0
    else:
        val_sum[maxi]=np.nan
    max2i=np.nanargmax(val_sum) #index of second strongest element
    max2v=val_sum[max2i]
    ratio=maxv/max2v
    if ratio<10 and debug:
        print('Warning: the strongest train is only {:.3f}x stronger then 2nd strongest! (using {:s})'.format(ratio,diag))

    shotTrainId=tr[maxi]
    #caching, to speed up things
    if use_cache:
        mu.dumpPickle([shotTrainId,ratio],fn)
        mu.grant_all(fn+'.pickle')
        if debug:
            print('shot trainId saved to local cache.')
    return shotTrainId, ratio, val_sum_o

def get_fullname(nickname):
    diag=''
    for d in diags:
        if d[0]==nickname:
            diag=d[1]
    if diag=='':
        print('diag_nickname not in dictionary!')
    return diag

def print_log(message,filename='auto_analysis_log.txt'):
    from datetime import datetime
    now = datetime.now() # current date and time
    date_time = now.strftime("%Y-%m-%d %H:%M:%S")
    st=date_time+': '+message
    print(st)
    file1 = open(filename, "a")
    file1.write(st+"\n")
    file1.close()
    os.chmod(filename, 0o777)



def apply_thresholds(array,threshold_lower=-1,threshold_upper=-1):
    if threshold_lower>0:
        fl=array<threshold_lower
        array[fl]=0
    if threshold_upper>0:
        fl=array>threshold_upper
        array[fl]=0
    return array

def get_array(run,diag_nickname='',train_selection='',PPU=[],selection_mean=1,diag='',source='',key='',debug=0,recalc=0,save=True,is_image=0,threshold_lower=-1,threshold_upper=-1,denoised=''):
    """
    Similar like get image, just more general for any arrays

    The inputs are complicated, see documentation on wiki.
    """
    global scratchDir
    global local

    ss=''
    trainId_offset=0
    if source=='':
        for d in diags:
            if d[0]==diag_nickname:
                diag=d[1]
                if np.size(d)>3:
                    key=d[3]
                trainId_offset=d[2]
        if diag=='':
            print(diag_nickname+' not in dictionary!')
            return np.array([]),np.array([])
    else:
        diag=source
    if not local:
        if not diag in run.all_sources:
            if debug:
                print(diag+" not in all_sources.")
            return np.array([]),np.array([])
    typetrainsel=type(train_selection)
    trainId=0
    if typetrainsel==str:
        if train_selection!='':
            ss='_'+train_selection
            if train_selection=='all': #disable cache
                recalc=1
                save=0
                selection_mean=0
            if train_selection=='mean': #use all and average them
                selection_mean=1
        elif np.size(PPU)>0:
            ss='_PPU'
#        else:   #those are outdated, might be removed later
#            if trainId>0:
 #               ss='_trainId{:03.0f}'.format(trainId)
  #          elif trainId==0:
   #             ss='_all'
    #        elif average:
     #           ss='_mean'
      #      else:
       #         ss='_sum'
    elif typetrainsel in [int,np.uint64,np.int64] :
        trainId=train_selection
        ss='_trainId{:03.0f}'.format(trainId)
    elif typetrainsel==np.ndarray:
        #disabling the cache
        realc=1
        save=0
    else:
        assert 0, 'Ohps, I dont know what to do with this train selection type.'
    if threshold_lower>0:ss=ss+'_thl{:.0f}'.format(threshold_lower)
    runNo,propNo,proc=get_run_info(run)
    if not proc:
        ss=ss+'_raw'
    if is_image: ext='tif'
    else: ext='pickle'

    if denoised!='': denoised_str='_denoised_'+denoised
    else: denoised_str=''
    fname="/p{:04.0f}_r{:04.0f}_{:s}{:s}{:s}.{:s}".format(propNo,runNo,diag_nickname,ss,denoised_str,ext)
    fn=scratchDir+"/"+fname
    if debug:
        print('** get_array')
        print(fn)
        print(diag)

    if recalc:
        load_now=1
    else:
        if os.path.isfile(fn):
            if debug:
                print('  Cache file found, loading')
                print(fn)
            if is_image:
                jungf = Image.open(fn)
                image = np.array(jungf)
                trains=-2
            else:
                image,trains=mu.loadPickle(fn)
            if debug:
                print("Data loaded. "+diag_nickname)
            load_now=0
        else:
            if debug: print('  Cache file not found')
            if local:
                global use_sftp
                global sftp_connection
                load_now=0
                if use_sftp:
                    try:
                        print('Downloading {:}'.format('/home/msmid/GPFS/exfel/exp/HED/202202/p003129/scratch/extra_mmm/'+fname))
                        sftp_connection.get('/home/msmid/GPFS/exfel/exp/HED/202202/p003129/scratch/extra_mmm/'+fname)
                        if debug: print('done')
                        os.rename("./"+fname, fn)
                        if debug: print('  SFTP moving')
                        if is_image:
                            jungf = Image.open(fn)
                            image = np.array(jungf)
                            trains=-2
                        else:
                            image,trains=mu.loadPickle(fn)
                        if debug:
                            print("Data loaded from SFTP., load_now is {:} ".format(load_now))
                    except Exception as E:
                        print('  neither on sftp')
                        print (E)
                        return np.array([]),np.array([])
                else:
                    print('Desired data not found in local cache.')
                    print('({:s})'.format(fn))
                    return np.array([]),np.array([])
            elif denoised!='':
                print('Denoised file not found. No data. Sorry.')
                return np.array([]),np.array([])
            else: load_now=1

    if load_now:   #Trying to get the data from Karabo system
        if key in run.keys_for_source(diag):  #Is the diagnostics here at all??
            if (np.size(PPU)==0) and (trainId>0):  #Getting one train only. this is the new elegant way based on https://extra-data.readthedocs.io/en/latest/iterate_trains.html
#                print('now here, doing traind {:}'.format(trainId))
                sel = run.select(diag,key)
                trains, data = sel.train_from_id(trainId)
                if key in data[diag]:
                    images=data[diag][key]
                else:
                    if debug:
                        print('Warning: Diagnotics {:s} has no key {:s} for given train, therefore providing no data.'.format(diag,key))
                    return np.array([]),np.array([])
                if 'XGM' in diag or 'data.peaks' in key :# or 'ATT' in diag : #my value is only first element, discard rest
                    images=images[0]
                images=np.squeeze(images)
                image=apply_thresholds(images,threshold_lower,threshold_upper)
#                print(np.sum(images))
                trains=trainId
            else:#  Loading all the images.The old way -- maybe I can later use the new one as well
                images=run.get_array(diag,key)
                trains=images.trainId.values
                if 'XGM' in diag or 'data.peaks' in key :# or 'ATT' in diag : #my value is only first element, discard rest
                    images=images[:,0]
                number_of_images=np.shape(images)[0]
                if debug: print("%d images in run"%(number_of_images))
                if number_of_images==0:
                    if debug:
                        print(diag+" no images in run.")
                    return np.array([]),np.array([])
                        #And now we will select the images
                use_some_selection=1
                if typetrainsel==str and train_selection!='': #using some of predefined train selections
                    if debug: print('Using train selection {:}'.format(train_selection))
                    if train_selection=='PPU':
                        seltrains=get_PPU_open(run)
                    elif train_selection[0:2]=='JP':
                        seltrains=get_jet_pointing_good_trains(run,train_selection)
                    elif train_selection in ['mean','all','sum']:
                        seltrains=[0]
                        use_some_selection=0
                    else:
                        assert 0,'Train selection not valid'
                elif np.size(PPU)>0:  #deprecated
                    if debug:  print('using PPU, {:.0f}'.format(np.size(PPU)))
                    seltrains=PPU
                elif typetrainsel==np.ndarray:
                    seltrains=train_selection
                else:
                    use_some_selection=0

                if use_some_selection:
                    selection=np.searchsorted(trains,seltrains+trainId_offset)
                    selection=selection[selection<np.shape(images)[0]]
                    images=images.data[selection]
                    images=apply_thresholds(images,threshold_lower,threshold_upper)
                    trains=seltrains
                else:  #using all
                    #image=images.data
                    images=apply_thresholds(images.data,threshold_lower,threshold_upper)
                    trains=trains

                if train_selection=='sum':
                    image = np.sum(images,axis=0)
                    trains=-1
                elif selection_mean:
                    if debug:print('doing selection mean')
                    image = np.mean(images,axis=0)
                    trains=-1
                else:
                    image=images
#                else:
 #                   trains=PPU
                image=np.squeeze(image)

                if 0:  #fragment of some old code
                        images=images.values
                        images=apply_thresholds(images,threshold_lower,threshold_upper)
                        if selection_mean:
                            image = np.mean(images,axis=0)
                        else:
                            image = np.sum(images,axis=0)
                        image = np.squeeze(image)
                        trains=-1
        else:
            if debug:
                print('Warning: Diagnotics {:s} has no key {:s}, therefore providing no data.'.format(diag,key))
            return np.array([]),np.array([])



        if save:
            if is_image:
                ImSave(image,fn,debug=debug)
            else:
                mu.dumpPickle([image,trains],fn)
            mu.grant_all(fn)

    return image,trains

def get_image_PPU(run,diag_nickname,threshold_lower=0,debug=0,denoised=''):
    PPU_open=get_PPU_open(run)
    im=get_image(run,diag_nickname=diag_nickname,PPU=PPU_open,debug=debug,threshold_lower=threshold_lower,denoised=denoised)
  #  if 'JF' in diag_nickname and denoised=='' and np.size(im)>1:
 #       im = remove_jungfrau_stripes(im)
#        im = np.transpose(im)
    return im

def get_image(run,diag_nickname,train_selection='',PPU=[],diag='',recalc=0,threshold_lower=-1,threshold_upper=-1,debug=0,save=True,denoised=''):
    im, tr = get_array(run=run,train_selection=train_selection,diag_nickname=diag_nickname,PPU=PPU,diag=diag,recalc=recalc,threshold_lower=threshold_lower,threshold_upper=threshold_upper,debug=debug,save=save,is_image=1,denoised=denoised)
    return im

def find_intersect_trains(*argv):
    inter=argv[0]
    for arg in argv:
        inter=np.intersect1d(inter,arg)
    return inter


def get_all_trains(run,debug=0):
    numtrains=run.get_array('HED_XTD6_PPU/MDL/PPU_TRIGGER','trainTrigger.numberOfTrains.value')
    trains=numtrains.trainId.values
    return trains




def get_PPU_open(run,return_bool_array=0,use_cache=1,debug=0):
    if debug: print('**get ppu open')
    if use_cache:
        runNo,propNo,proc=get_run_info(run)
        fname="p{:04.0f}_r{:04.0f}_PPU_OPEN{:}.pickle".format(propNo,runNo,return_bool_array)
        fn=scratchDir+"/"+fname
        pick=mu.loadPickle(fn,safe=0)
        if pick!=0:
            if return_bool_array:
                return pick[0],pick[1],pick[2]
            else:
                return pick[0]
        else:
            if 1:#transitional workaround - trying to read PPU without the reaturn bool array parameter, assuming before was return_bool_array=0
                fname="p{:04.0f}_r{:04.0f}_PPU_OPEN.pickle".format(propNo,runNo)
                fn=scratchDir+"/"+fname
                pick=mu.loadPickle(fn,safe=0)
#                print('loading trans.:' +fn)
                if pick!=0:
                    assert not return_bool_array, 'you do a bad things with get_PPU open now!'
                    return pick[0]

            global use_sftp
            global sftp_connection
            if use_sftp:
                print('useftp')
                print(fname)
                try:
#                    if 1:#very bad transitional workaround - trying to read PPU without the reaturn bool array parameter, assuming before was return_bool_array=0
 #                       fname="p{:04.0f}_r{:04.0f}_PPU_OPEN".format(propNo,runNo)
                    print('Trying to download {:}'.format('/home/msmid/GPFS/exfel/exp/HED/202202/p003129/scratch/extra_mmm/'+fname))
                    sftp_connection.get('/home/msmid/GPFS/exfel/exp/HED/202202/p003129/scratch/extra_mmm/'+fname)
                    if debug: print('done')
                    os.rename("./"+fname, fn)
                    if debug:
                        print("Data loaded from SFTP. ")
                    pick=mu.loadPickle(fn)
                    if return_bool_array:
                        return pick[0],pick[1],pick[2]
                    else:
                        return pick[0]
                except Exception as E:
                    print('  neither on sftp, error:')
                    print (E)
                    print('end of error')

            else:
                if debug:
                    print('PPU could not be loaded from local cache.')
    global local
    if not local:
        if not 'HED_XTD6_PPU/MDL/PPU_TRIGGER' in run.all_sources:
            return [],[],[]
    else:
        print('Could not get PPU from local cache')
        return [],[],[]
    seqstart=run.get_array('HED_XTD6_PPU/MDL/PPU_TRIGGER','trainTrigger.sequenceStart.value')
    numtrains=run.get_array('HED_XTD6_PPU/MDL/PPU_TRIGGER','trainTrigger.numberOfTrains.value')
    starts=np.unique(seqstart.values) #unique values of sequence start
    inRun=(starts>=np.min(seqstart.trainId.values))
    starts=starts[inRun] #unique values of sequence start actually within the run
#    indexes=(numtrains.trainId.values==starts)
    indexes=np.searchsorted(numtrains.trainId.values, starts)

    num_trains_on_start=numtrains.values[indexes] #values of 'numberOfTrains' correspnding to 'starts'
    if debug:
        if 0:
            print('seqstart')
            print(seqstart)
            print('numtrains')
            print(numtrains)
        print('starts')
        print(starts)
        print('indexes')
        print(indexes)
        print('num_trains_on_start')
        print(num_trains_on_start)

    if np.size(num_trains_on_start)!=np.size(starts):
        print('Something weird with PPU')
        return [],[],[]
    PPU_open=np.zeros(0) #createing empty array
    for i,start in enumerate(starts):
        PPU_open=np.append(PPU_open,np.arange(start,start+num_trains_on_start[i]))

    trains=numtrains.trainId.values
    PPU_open_bool=trains*0
    indexes=np.searchsorted(trains, PPU_open)
    indexes=indexes[indexes<np.size(PPU_open_bool)]  #cut off what is later
  #  print(indexes)
    PPU_open_bool[indexes]=1

    if use_cache:
        mu.dumpPickle([PPU_open,trains,PPU_open_bool],fn)
        mu.grant_all(fn+'.pickle')
        if debug:
            print('PPU saved to local cache.')

    if return_bool_array:
        return PPU_open,trains,PPU_open_bool #PPU_open is an array containing trainIds when the PPU is open
    else:
        return PPU_open #PPU_open is an array containing trainIds when the PPU is open




#def merge_dataarrays_by_trains(*argv)
if 0:
    inter=argv[0]
    for arg in argv:
        trains=arg.trainId
        inter=np.intersect1d(inter,arg)
    #return inter
    pos,trainsPos=em.get_array_raw(run,"LC_AXIS_4")   # Motor which was scanning
    jf,trainsJF =em.get_array_raw(run,diag)   #Images to get
    xgm_orig,xgm_tr=em.get_array_raw(run,'XGM_SA2')
    xgm_orig=xgm_orig[:,0]
    #xgm_hed,xgm_hed_tr=em.get_array_raw(run,'XGM_HED')
    #xgm_orig=xgm_hed[:,0]

    trains_all = em.find_intersect_trains(xgm_tr,trainsJF,trainsPos)

    posSel=pos[np.searchsorted(trainsPos, trains_all)]
    xgm=xgm_orig[np.searchsorted(xgm_tr, trains_all)]
    jf=jf[np.searchsorted(trainsJF, trains_all)]


def get_run_info_old(run,debug=0): #using metadata, does not work reliably
    if 'runNumber' in run.run_metadata():
        runNo=run.run_metadata()['runNumber']
    else:
        if debug: print("Couldn't read run number.")
        runNo=0
    if 'proposalNumber' in run.run_metadata():
        proposalNo=run.run_metadata()['proposalNumber']
    else:
        if debug: print("Couldn't read proposal number.")
        proposalNo=0
    proc=0
    for f in run.files:
        yes=('proc' in f.filename)
        if yes:
            proc=1
            break

    return runNo,proposalNo,proc

def get_run_info(run,debug=0): #based on advice from DA-support
    global local
    if local:
        global local_run_info
#        local_run_info=[proposal,runNo,data]
        return local_run_info
    else:
        filename=run.files[0].filename
        if debug:
            print(filename)
        spl=filename.split("/")
        if "OVERVIEW" in filename:
            runNo= int(filename.split("-")[1][1:])
            proposal= int(spl[6][1:])
            proc= (spl[7]=='proc')

        else:
            runNo= int(spl[8][1:])
            proposal= int(spl[6][1:])
            proc= (spl[7]=='proc')
        proc=1 #the original way did not work, I'm assuming from now I'm always using 'proc'
        global online_proc
        if online_proc!=-1:
             proc=online_proc
    return runNo,proposal, proc

def remove_jungfrau_stripes(im):
    im[512:523,:]=im[512:523,:]/2
    im[:,511:513]=im[:,511:513]/2
    im[255:257,:]=im[255:257,:]/2
    im[:,255:257]=im[:,255:257]/2
    return im

def ExpDir(proposal):
    """
    Simple function to return formatted experiment directory based on proposal number. by Olly Humphries
    """
    return extra_data.read_machinery.find_proposal('p{:06d}'.format(proposal))


def LoopAttempts(PropNum:int, FunctionToRun:callable, RunsToExclude:list=[],FigFolder = 'Figures',TimeToWait = 60,SleepTime=10):
    """
    Inputs:
    PropNum:int
        Experiment proposal number
    FunctionToRun:callable
        function accepting argument of run number
    RunsToExclude:list
        Any bad runs/very long dark runs to avoid, with no useful data
    FigFolder:string
        Relative folder where eventual output of already processed runs (which are excluded then) is stored
    TimeToWait: int
        How long [s] has the data folder be intact to start processing
    SleepTime: int
        How long [s] to wait before another attempt
    written by Oliver Humphries
    """
    ProposalDirectory = extra_data.read_machinery.find_proposal('p{:06d}'.format(PropNum))
    while 1:
        RunsWithData = set(int(s[-4:]) for s in glob.glob(os.path.join(ExpDir(),'proc','r*')))
        RunsWithFigures = set(int(s[-8:-4]) for s in glob.glob(os.path.join(FigFolder,'*'+4*'[0-9]'+'.png')))
        RunsToGenerate = sorted([r for r in RunsWithData if r not in RunsWithFigures and r not in RunsToExclude])
        for RunNumber in RunsToGenerate[::-1]:
            try:
                filedirs = [os.path.join(ProposalDirectory,'proc',f'r{RunNumber:04.0f}'),
                            os.path.join(ProposalDirectory,'raw',f'r{RunNumber:04.0f}')]
                files = [glob.glob(os.path.join(fdir,'*.h5')) for fdir in filedirs]
                fnames = [fname for flist in files for fname in flist]
                if all([(time.time() - os.path.getmtime(fname)) > TimeToWait for fname in fnames]):
                    print(f'Processing run {RunNumber}')
                    FunctionToRun(RunNumber)
                break
            except FileNotFoundError:
                print(f'No processed data for run {RunNumber}')
            except Exception as E:
                RunsToExclude.append(RunNumber)
        time.sleep(SleepTime)




def wait_for_shot(proposal,outfile_mask,debug=0,SleepTime=3,TimeToWait=40,RunsToExclude=[]):
    """
    something like LoopAttempts by Olly, but adapted to Michal to fit his framework...
    TimeToWait: int
    How long [s] has the data folder be intact to start processing
    SleepTime: int
    How long [s] to wait before another attempt
    """
    print_log('Starting to wait.')
    i=1
    ProposalDirectory = extra_data.read_machinery.find_proposal('p{:06d}'.format(proposal))
    if debug: print(ProposalDirectory)
    breaking=0
    while 1:
        RunsWithData = set(int(s[-4:]) for s in glob.glob(os.path.join(ExpDir(proposal),'proc','r*')))
#        RunsWithFigures = set(int(s[-8:-4]) for s in glob.glob(os.path.join(FigFolder,'*'+4*'[0-9]'+'.png')))
        FigFiles=outfile_mask
        RunsWithFigures=[]
        for s in (glob.glob(os.path.join(FigFiles))):
            prop1=s.split('_')[1][1:]
            if int(prop1)!=proposal: continue
            if s.find('shot')>=0 : continue
            run1=s.split('_')[2][1:]
            pp= run1.find('.')
            if pp>0: run1=run1[:pp]
            RunsWithFigures.append(int(run1))

        RunsToGenerate = sorted([r for r in RunsWithData if r not in RunsWithFigures and r not in RunsToExclude])
       #RunsToGenerate = sorted([r for r in RunsWithData if r not in RunsWithFigures])
        if debug:
            print('RUNS: wData / exclude / With figures /// To generate:')
            print(RunsWithData)
            print(RunsToExclude)
            print(RunsWithFigures)
            print(RunsToGenerate)
        for RunNumber in RunsToGenerate:
            try:
                filedirs = [os.path.join(ProposalDirectory,'proc',f'r{RunNumber:04.0f}'),
                            os.path.join(ProposalDirectory,'raw',f'r{RunNumber:04.0f}')]
                files = [glob.glob(os.path.join(fdir,'*.h5')) for fdir in filedirs]
                fnames = [fname for flist in files for fname in flist]
                times=([(time.time() - os.path.getmtime(fname)) for fname in fnames])
                tin=np.min(times)
                print_log('There are {:.0f} files for run {:.0f}, the youngest is {:.0f} s old.'.format(np.size(files),RunNumber,tin))
                #print(times)
                if all([(time.time() - os.path.getmtime(fname)) > TimeToWait for fname in fnames]):
                    print_log('Going to process run #{:.0f}'.format(RunNumber))
                    breaking=1
                    break
                break
            except FileNotFoundError:
                print_log(f'No processed data for run {RunNumber}')
 #           except Exception as E:
#                RunsToExclude.append(RunNumber)
            print('here')
            if breaking : break
        if breaking : break
        time.sleep(SleepTime)
        print(".",end='')
        if np.mod(i,20)==0:
            print_log('')
            print_log("{:.0f} min.".format(i/20))
        i=i+1
    return RunNumber

def listen_for_shot(tcpline='tcp://hed-br-kc-comp-1:10344',debug=0):
    """
    from Philippo
    """
    import zmq
    import json
    s = zmq.Context.instance().socket(zmq.SUB)
    #s.connect('tcp://10.253.0.172:10343')#old
#    tcpline='tcp://hed-br-kc-comp-1:10343'
 #   tcpline='tcp://hed-br-kc-comp-1:10344'
    print('Listening to :'+tcpline)
    s.connect(tcpline)# seen in 2023 / march
    s.subscribe(b'cal')
    from datetime import datetime
    now = datetime.now()
    current_time = now.strftime("%H:%M")
    print_log(' Listening...')
    _, raw_json = s.recv_multipart()
    data = json.loads(raw_json.decode('ascii'))
    if debug:
        print_log(data)
    runNo=data['run']
    current_time = now.strftime("%H:%M")
    print_log(": ...I've heard there was run #{:.0f}".format(runNo))
    return runNo


def format1(i):
    if i >1e4:
        f='{:.1e}'
    elif i>7:
        f='{:.0f}'
    elif i>0.7:
        f='{:.2f}'
    elif i>0.07:
        f='{:.2f}'
    elif i>0.007:
        f='{:.3f}'
    elif i>0.0007:
        f='{:.4f}'
    else:
        f='{:.2e}'
    return f.format(i)

def get_IPM_OPT(run,use_PPU=0,PPU=[],trainId=-1,debug=0,calibration='3129',train_selection=''):
    if use_PPU:
        PPU=get_PPU_open(run)
    ipm_opt5,tr=get_array(run,'IPM_OPT_ch5',PPU=PPU,debug=debug,train_selection=train_selection)
    ipm_opt2,tr=get_array(run,'IPM_OPT_ch2',PPU=PPU,debug=debug,train_selection=train_selection)
    if np.size(ipm_opt5)==0:
        return np.nan,np.nan
    if calibration=='2022-07':
         #Calibration from July 2022, based on data from proposal 2854 for 8150 eV
        ipm_opt4,tr=get_array(run,'IPM_OPT_ch4',PPU=PPU,train_selection=train_selection)
        ipm_opt3,tr=get_array(run,'IPM_OPT_ch3',PPU=PPU,train_selection=train_selection)
        ipm_cal3=ipm_opt3/-26.7
        ipm_cal2=ipm_opt2/-80.1
        ipm_cal=ipm_cal2

        if np.size(ipm_cal)>1:
            ipm_cal[ipm_cal>500]=ipm_cal3[ipm_cal>500]
        else:
            if ipm_cal>500: ipm_cal=ipm_cal3

    if calibration=='2022-08':
     #Calibration from August 2022, based on data from proposal 3129 for 8200 eV, based on run #
    #runs 53 and 55, calibrated through XGM_HED
        ipm_opt4,tr=get_array(run,'IPM_OPT_ch4',PPU=PPU,train_selection=train_selection)
        ipm_opt3,tr=get_array(run,'IPM_OPT_ch3',PPU=PPU,train_selection=train_selection)
        ipm_cal2=ipm_opt2/-358.66710356
        #ipm_cal3=ipm_opt3/-358.66710356
        ipm_cal4=ipm_opt4/-116.27
        ipm_cal5=ipm_opt5/-24.719
        if np.size(ipm_cal5)>1:
            ipm_cal=ipm_cal2
            ipm_cal[ipm_opt2<-55000]=ipm_cal5[ipm_opt2<-55000]
        else:
            ipm_cal=ipm_cal2
            if ipm_opt2<-55000:
                ipm_cal=ipm_cal5

    if calibration=='3129':
     #Calibration for proposal 3129 for 8200 eV, based on run #runs 53 and 54, calibrated through XGM_HED
    #described in https://gitlab.hzdr.de/relax-xfel/xfel-beamline/-/wikis/27.9.2022%20transmission%20analysis%20and%20IPM%20calibration
        ipm_cal2=ipm_opt2/-299.9
        ipm_cal5=ipm_opt5/-24.7
        if np.size(ipm_cal5)>1:
            ipm_cal=ipm_cal2
            ipm_cal[ipm_opt2<-55000]=ipm_cal5[ipm_opt2<-55000]
        else:
            ipm_cal=ipm_cal2
            if ipm_opt2<-55000:
                ipm_cal=ipm_cal5

    if calibration=='3430':
     #just a guess for veyr ryough numbers during 3430
        ipm_cal=ipm_opt2/-299.9/67*1037
    if debug:
        print('  get_IPM_OPT')
    return ipm_cal,tr

def get_first_train_IPM(run,threshold=0.5,draw=0,debug=0):
    ipm,tr=get_IPM_OPT(run,trainId=0,debug=debug)
    first_train=np.min(tr[ipm>threshold])
    if draw:
        mu.figure()
        plt.semilogy(tr,ipm)
        plt.ylim(1e-4,1000)
        plt.vlines(first_train,1e-4,1000,'k',lw=1)
        #plt.xlim(first_train-100,first_train+100)
    return first_train

def get_shot_trainId_IPM(run,ratio_thr=10,debug=0):
#written Aug2022 in preparation for 3192
    ipm,tr=get_IPM_OPT(run,trainId=0,debug=debug)
    maxi=np.nanargmax(ipm)  #index of strongest element
    maxv=ipm[maxi]
    ipm[maxi]=0
    max2i=np.nanargmax(ipm) #index of second strongest element
    max2v=ipm[max2i]
    ratio=maxv/max2v
    if ratio<ratio_thr:
        if debug:
            print('The strongest train is only {:.3f}x stronger then 2nd strongest! (using IPM)'.format(ratio))
        trainId=-1
    else:
        if debug:
            print('shot ratio good enough ({:.0f}x)'.format(ratio))
        trainId=tr[maxi]
    return trainId, ratio



print_text_x=0.02
print_text_y=0.8
print_text_rw=0.05
global print_text_row
print_text_row=0


def print_text(text,value,row=-1,fs=14,derived=0,manualformat=0):
    x=print_text_x
    if derived: x=x+0.05
    if row==-1:
        global print_text_row
        row=print_text_row
        print_text_row=print_text_row+1
    if np.size(value)==0:
        if manualformat:
            textnull=text
        else:
            pos=text.find('{')
            textnull=text[:pos]+'n/a'
        col='red'
        if derived: col =[0.7,0.3,0.3]
        plt.gcf().text(x,print_text_y-print_text_rw*row, textnull, fontsize=fs,color=col)
    else:
        if manualformat:
            text2=text
        else:
            text2=text.format(format1(value))
        col='black'
        if derived: col=[0.4,0.4,0.4]
        plt.gcf().text(x,print_text_y-print_text_rw*row, text2, fontsize=fs,color=col)


def get_last_profile(runNo,offset=0):
    files=glob.glob('./lineouts/saxs_prof*')
    files=np.sort(files)
    for i in np.flip(np.arange(np.size(files))):
        run=int(files[i][20:24])
        if run<runNo-offset:
            break
    res=mu.loadPickle(files[i])
    if res !=0:
        a,b,c,d=res
    else:
        return 0,0,0,0,0
    return a,b,c,d,run
#return prx,plofs,profs

def transmission_check(run,diag1,diag2,forcezero=1,polyfit_order=1):
    runNo,dd,ss=get_run_info(run)
    d1,tr1 = get_array(run,diag1,trainId=0)
    d2,tr2 = get_array(run,diag2,trainId=0)
    tr=find_intersect_trains(tr1,tr2)
    d1=d1[np.searchsorted(tr1, tr)]
    d2=d2[np.searchsorted(tr2, tr)]

    nz=int(forcezero*1e5)
    d1=np.append(d1,np.zeros(nz))
    d2=np.append(d2,np.zeros(nz))

    pv=np.polyfit(d1,d2,polyfit_order)

    mu.figure(13,5)
    plt.subplot(131)
    plt.title('Run #{:.0f}'.format(runNo))
    plt.plot(d1,d2,'*',alpha=0.2)
    plt.xlabel(diag1)
    plt.ylabel(diag2)


    plt.subplot(132)
    plt.title('d2 ={:.2e} * d1 + {:.02e}'.format(pv[0],pv[1]))
    d2_cal=(d2-pv[1])/pv[0]
    plt.plot(d1,d2_cal,'*',alpha=0.2,label='channel 5')
    mi=np.min(d1)
    ma=np.max(d1)
    plt.plot([mi,ma],[mi,ma],'k-',lw=1)
    plt.xlabel(diag1)
    plt.ylabel(diag2+', calibrated to '+diag1)

    plt.subplot(133)
    precision=(d2_cal-d1)/d1*100
    precision[np.logical_not(np.isfinite(precision))]=np.nan
    edges=np.arange(-5,5,0.35)
    a,b=np.histogram(precision,edges)
    plt.plot(b[:-1],a)
    #plt.ylim(0,70)
    plt.grid()
    plt.xlabel('Deviation [%]')
    plt.title('Mean deviation: {:.2f}%'.format(np.nanmean(np.abs(precision))))
    plt.ylabel('Number of trains')
    plt.tight_layout()
    return pv

def save_all_trains(run,diag_nickname='Zyla',threshold_lower=0,debug=0,maxcnt=1e9): #saves images for all trains into cache
    print("Trying to load all trains into memory.")
    images,trains=get_array(run,diag_nickname,trainId=0,save=False,debug=debug)
    numtrains=np.size(trains)
    ext='tif'
    runNo,proposal,proc=get_run_info(run)
    print("There were {:.0f} trains".format(numtrains))
    for i,trainId in enumerate(trains):
        print(i)
        if i>maxcnt:
            break
        image=images[i,:,:]
        image=apply_thresholds(image,threshold_lower)
        ss='_trainId{:03.0f}'.format(trainId)
        if threshold_lower>0:ss=ss+'_thl{:.0f}'.format(threshold_lower)
        if not proc:
            ss=ss+'_raw'
        fn=scratchDir+"/p{:04.0f}_r{:04.0f}_{:s}{:s}.{:s}".format(proposal,runNo,diag_nickname,ss,ext)
        ImSave(image,fn,debug=debug)
    print('Done saving all files')

# cryojet things for proposal 4446

def get_jet_pointing_good_trains(run,train_selection='JP',return_bool_array=0,return_distance=0):
    assert train_selection[0:2]=='JP', 'get jet positions nonsense 1'
 #   print(train_selection)
#    print(np.size(train_selection))
    if len(train_selection)>2:  #assuming format JP_xx_yy_radius
        spl=train_selection.split('_')
        invert=spl[0]=='JPnot'
        xx=int(spl[1])
        yy=int(spl[2])
        allowed_hit_deviation=int(spl[3])
    else:
        invert=0
        xx=0
        yy=0
        allowed_hit_deviation=10

    jp=get_jet_positions_parallel(run)
    jp2=jp[:,1:3]
    jp2[:,0]+=xx
    jp2[:,1]+=yy
    norma=np.linalg.norm(jp2,axis=1)
    if not invert:
        sel=norma<allowed_hit_deviation
    else:
        sel=norma>=allowed_hit_deviation
    goodtrains=jp[sel,0]
    if return_bool_array:
        return sel
    if return_distance:
        return sel,norma
    return goodtrains

def get_jet_positions(run,debug=0,use_cache=1,maxtrains=100000000,reload=0):
    runNo,propNo,proc=get_run_info(run)
    fn=scratchDir+"/p{:04.0f}_r{:04.0f}_jetpositions".format(propNo,runNo)
    if use_cache and not reload:
        pick=mu.loadPickle(fn,safe=0)
        if np.size(pick)>1:
            if debug:
                print('Jet positions loaded from local cache.')
            return pick

    import metro_context_em as mc

    trains=get_all_trains(run)
    trains=trains[0:maxtrains]
    print('Getting jet positions')
    jet_positions=get_beam_pointing_chunk(trains,run,show_progress=1)
    if 0:
        numtrains=np.size(trains)
        jet_positions=np.zeros((np.size(trains),3))*np.nan
        pprog=-10
        for ti,trainId in enumerate(trains):
            if ti>pprog+10:
                mu.update_progress(ti,numtrains)
                pprog=ti
            cams=['CAM1','CAM2']
            jet_positions[ti,0]=trainId
            for ci,cam in enumerate(cams):
                img=get_image(run,cam,save=False,train_selection=trainId)
                if np.size(img)==0:continue
                data=xr.DataArray(img)
                jet_x=get_beam_pointing1(cam,data)
                if debug:
                    jet_fit=jp.jet_fit(bgs,perp_dist,jet_present)
                    print(jet_x)
                    mu.figure()
                    plt.imshow(jet_fit.transpose())
                jet_positions[ti,ci+1]=jet_x
    #caching, to speed up things
    if use_cache:
        mu.dumpPickle(jet_positions,fn)
        mu.grant_all(fn+'.pickle')
        if debug:
            print('Jet positions saved to cache.')
    return jet_positions



def get_beam_pointing1(cam,data):
    jp = mc.JetPosition(cam)
    if cam=='CAM1':
        jp.reshape=False
        jp.mask_file=''
        jp.blur_kernel=81
        jp.beam_pixel_height=2175
        jp.beam_pixel_x=982
        jp.jet_det_thresh=0.3
        jp.line_offset=5
        jp.roi_x=slice(800,1200)
        jp.roi_y=slice(1000,3500)
        jp.um_per_pixel= 0.361
    if cam=='CAM2':
        jp.reshape=False
        jp.mask_file='cam2_mask.npy'
        jp.blur_kernel=81
        jp.beam_pixel_height=1480
        jp.beam_pixel_x=2151
        jp.jet_det_thresh=100
        jp.jet_det_thresh=0.3
        jp.line_offset=30
        jp.roi_x=slice(1500,2300)
        jp.roi_y=slice(0,2000)
        jp.um_per_pixel= 0.1225
    #this is in Imager
    ds=jp.data_shape(data)
    raw=jp.raw(data)
    #background=jp.background(raw)
    background=raw*0
    mask=jp.mask(ds)#
    bgs=jp.bg_subtracted(raw,mask,background)#
    blur=jp.blur(bgs)#
    threshold=jp.threshold(blur,mask)#
    foreground=jp.foreground(bgs,threshold)#

    median_foreground=jp._median_foreground(foreground)#
    line_fit=jp.line_fit(median_foreground)#
    if (line_fit==None): return np.nan
    perp_dist=jp.perpendicular_distance(line_fit,bgs)#
    jet_absorption=jp.jet_absorption(bgs,perp_dist,mask)#
    jet_present=jp.jet_present(jet_absorption)#
    jet_x=jp.jet_x(line_fit,jet_present)   #
    if 0:
        if 0:
            mu.figure()
            plt.imshow(foreground)
            plt.colorbar()
            plt.title(jet_x)
        print("Raw mean: {:}".format(np.mean(raw).data))
        print("Fg mean: {:}".format(np.mean(foreground).data))
        print("Line fit: {:}".format(line_fit))
        print("Jet absorption: {:}".format(jet_absorption.data))
        print("Result now: {:}".format(jet_x))
    return jet_x


def get_beam_pointing_chunk(trainId_chunks,run,show_progress=0):
        jet_positions=np.zeros((np.size(trainId_chunks),3))*np.nan
        cams=['CAM1','CAM2']
        pprog=0
        numtrains=np.size(trainId_chunks)
        for ti,trainId in enumerate(trainId_chunks):
            if show_progress:
                if ti>pprog+10:
                    mu.update_progress(ti,numtrains)
                    pprog=ti

            jet_positions[ti,0]=trainId
            for ci,cam in enumerate(cams):
                #imgs,trains = get_array(run,cam,train_selection=np.asarray(trainId_chunks),save=False,selection_mean=0)
                img=get_image(run,cam,save=False,train_selection=trainId)
                if np.size(img)==0:continue
                data=xr.DataArray(img)
                jet_x=get_beam_pointing1(cam,data)
                jet_positions[ti,ci+1]=jet_x
        return jet_positions
#### From Thomas Kluge, the parallel version

def get_jet_positions_parallel(run,debug=0,trainIds=[-1],use_cache=1,maxtrains=100000000,reload=0,parallel = 10):

    def get_beam_pointing_chunkI(trainId_chunks,show_progress=0):
        jet_positions=np.zeros((np.size(trainId_chunks),3))*np.nan
        cams=['CAM1','CAM2']
        for ti,trainId in enumerate(trainId_chunks):
            jet_positions[ti,0]=trainId
            for ci,cam in enumerate(cams):
                #imgs,trains = get_array(run,cam,train_selection=np.asarray(trainId_chunks),save=False,selection_mean=0)
                img=get_image(run,cam,save=False,train_selection=trainId)
                if np.size(img)==0:continue
                data=xr.DataArray(img)
                jet_x=get_beam_pointing1(cam,data)
                jet_positions[ti,ci+1]=jet_x
        return jet_positions


    runNo,propNo,proc=get_run_info(run)
    fn=scratchDir+"/p{:04.0f}_r{:04.0f}_jetpositions".format(propNo,runNo)
    if use_cache and not reload:
        pick=mu.loadPickle(fn)
        if np.size(pick)>1:
            if debug:
                print('Jet positions loaded from local cache.')
            if len(trainIds) != 0:
                if trainIds[0] != -1:
                    return_array=[]
                    for indx in trainIds:
                        if indx in pick[:,0]:
                            i = np.where(pick[:,0]==indx)[0][0]
                            return_array.append(pick[i,:])
                    return np.asarray(return_array)
                else:
                    return pick
            return np.asarray([[0,0,0]])
    print('Getting jet positions, in parallel.')

    import metro_context_em as mc
    trains = get_all_trains(run)
    trains=trains[0:maxtrains]
    numtrains=np.size(trains)

    from multiprocess import Pool
    trainId_chunks = np.array_split(trains, parallel)
    output=np.array([0,0,0])
    with Pool(parallel) as p:
        pool_outputs = list(
            tqdm(
                p.imap(get_beam_pointing_chunkI,trainId_chunks),
                total=len(trainId_chunks),leave=False
            )
        )
        flattened_output = []
        for pool_output in pool_outputs:
#            flattened_output += pool_output
            output=np.vstack((output,pool_output))
        #pool_outputs = np.asarray(pool_outputs)
        #print(len(flattened_output),len(jet_positions[:,ci+1]))
    #    jet_positions[:,ci]=flattened_output#pool_outputs.flatten()[:-1]


    jet_positions=output[1:,:]
    #jet_positions[:,0]=trains
    #caching, to speed up things
    if use_cache:
        mu.dumpPickle(jet_positions,fn)
        mu.grant_all(fn+'.pickle')
        if debug:
            print('Jet positions saved to cache.')
    if len(trainIds) == 0: return np.asarray([[0,0,0]])
    if trainIds[0] != -1:
        trains=trainIds
        return_array=[]
        for indx in trainIds:
            if indx in jet_positions[:,0]:
                i=np.where(indx==jet_positions[:,0])[0][0]
                return_array.append(jet_positions[i,:])
        return np.asarray(return_array)

    return jet_positions

